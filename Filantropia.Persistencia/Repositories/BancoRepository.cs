﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class BancoRepository : Connection, IPersistencia<Banco>
    {
        public void Insert(Banco entity)
        {
            try
            {
                
                OpenConnection();

                string query = "insert into Bancos (Banco, Numero) values(@Banco, @Numero)";

                Cmd = new SqlCommand(query, Con);
                
                Cmd.Parameters.AddWithValue("@Banco", entity.NomeBanco);
                Cmd.Parameters.AddWithValue("@Numero", entity.CodigoBanco);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }

        }

        public void Update(Banco entity)
        {
            try
            {

                OpenConnection();

                string query = "update Bancos set Banco = @Banco,  Numero = @Numero where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@Banco", entity.NomeBanco);
                Cmd.Parameters.AddWithValue("@Numero", entity.CodigoBanco);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from bancos where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Banco> FindAll()
        {
            List<Banco> listBanco = new List<Banco>();
            Banco banco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from bancos;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    banco = new Banco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        NomeBanco = Convert.ToString(Dr["Banco"]),
                        CodigoBanco = Convert.ToInt32(Dr["Numero"])
                    };

                    listBanco.Add(banco);
                }

                return listBanco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Banco FindById(int id)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Bancos where ID = @id";
                
                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@id", id);
                Dr = Cmd.ExecuteReader();

                Banco c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Banco()
                    {
                        Identificador = (int)Dr["ID"],
                        NomeBanco = (string)Dr["Nome"],
                        CodigoBanco = (int)Dr["Numero"],
                    }; 
                    
                }
                return c; 
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection(); 
            }

        }

        public Banco FindByName(string name)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Bancos where Banco = @banco";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@banco", name);
                Dr = Cmd.ExecuteReader();

                Banco c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Banco()
                    {
                        Identificador = (int)Dr["ID"],
                        NomeBanco = (string)Dr["Nome"],
                        CodigoBanco = (int)Dr["Numero"],
                    };

                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        
    }
}
