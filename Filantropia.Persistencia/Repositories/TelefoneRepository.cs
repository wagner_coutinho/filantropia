﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class TelefoneRepository : Connection, IPersistencia<Telefone>
    {
        public void Insert(Telefone entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Telefones (DDD, Prefixo, Sufixo) " +
                    "values(@DDD, @Prefixo, @Sufixo)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@DDD", entity.DDD);
                Cmd.Parameters.AddWithValue("@Prefixo", entity.Prefixo);
                Cmd.Parameters.AddWithValue("@Sufixo", entity.Sufixo);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Telefone entity)
        {
            try
            {
                OpenConnection();

                string query = "update Telefones set DDD = @DDD, Prefixo = @Prefixo, Sufixo = @Sufixo where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@DDD", entity.DDD);
                Cmd.Parameters.AddWithValue("@Prefixo", entity.Prefixo);
                Cmd.Parameters.AddWithValue("@Sufixo", entity.Sufixo);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from Telefones where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Telefone> FindAll()
        {
            List<Telefone> listTelefone = new List<Telefone>();
            Telefone telefone = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Telefones;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    telefone = new Telefone()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        DDD = Convert.ToInt32(Dr["DDD"]),
                        Prefixo = Convert.ToString(Dr["Prefixo"]),
                        Sufixo = Convert.ToString(Dr["Sufixo"]),

                    };

                    listTelefone.Add(telefone);
                }

                return listTelefone;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Telefone FindById(int id)
        {
            Telefone telefone = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Telefones where id = @ID;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    telefone = new Telefone()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        DDD = Convert.ToInt32(Dr["DDD"]),
                        Prefixo = Convert.ToString(Dr["Prefixo"]),
                        Sufixo = Convert.ToString(Dr["Sufixo"]),

                    };

                }

                return telefone;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Telefone FindByName(string name)
        {
            throw new NotImplementedException();
        }


    }
}
