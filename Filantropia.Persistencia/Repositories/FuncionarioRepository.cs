﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Entidades.VO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class FuncionarioRepository : Connection, IPersistencia<FuncionarioVO>
    {
        public void Insert(FuncionarioVO entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Funcionarios (DataAdmissao, DataDesligamento, RG, IDTipoFuncionario, IDPessoa, Ativo, Senha, IDResponsavel) " +
                    "values(@DataAdmissao, @DataDesligamento, @RG, @IDTipoFuncionario, @IDPessoa, @Ativo, @Senha, @IDResponsavel)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@DataAdmissao", entity.DataAdmissao);
                Cmd.Parameters.AddWithValue("@DataDesligamento", entity.DataDesligamento);
                Cmd.Parameters.AddWithValue("@RG", entity.Identidade);
                Cmd.Parameters.AddWithValue("@IDTipoFuncionario", entity.IdTipoFuncionario);
                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@Ativo", entity.Ativo);
                Cmd.Parameters.AddWithValue("@Senha", entity.Senha);
                Cmd.Parameters.AddWithValue("@IDResponsavel", entity.IdResponsavelCadastro);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(FuncionarioVO entity)
        {
            try
            {
                OpenConnection();

                string query = "update Funcionarios set DataAdmissao = @DataAdmissao, DataDesligamento = @DataDesligamento, RG = @RG, IDTipoFuncionario = @IDTipoFuncionario, " +
                    "IDPessoa = @IDPessoa, Ativo = @Ativo, Senha = @Senha, IDResponsavelCadastro = @IDResponsavel where ID = @ID;";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@DataAdmissao", entity.DataAdmissao);
                Cmd.Parameters.AddWithValue("@DataDesligamento", entity.DataDesligamento);
                Cmd.Parameters.AddWithValue("@RG", entity.Identidade);
                Cmd.Parameters.AddWithValue("@IDTipoFuncionario", entity.IdTipoFuncionario);
                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@Ativo", entity.Ativo);
                Cmd.Parameters.AddWithValue("@Senha", entity.Senha);
                Cmd.Parameters.AddWithValue("@IDResponsavel", entity.IdResponsavelCadastro);

                Cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from Funcionarios where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<FuncionarioVO> FindAll()
        {
            List<FuncionarioVO> listFuncionario = new List<FuncionarioVO>();
            FuncionarioVO funcionario = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Funcionarios;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    funcionario = new FuncionarioVO()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
      
                        DataAdmissao = Convert.ToDateTime(Dr["DataAdmissao"]),
                        DataDesligamento = Convert.ToDateTime(Dr["DataDesligamento"]),
                        Identidade = Convert.ToString(Dr["RG"]),
                        IdTipoFuncionario = Convert.ToInt32(Dr["IDTipoFuncionario"]),
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        IdResponsavelCadastro = Convert.ToInt32(Dr["IDResponsavelCadastro"])
                    };

                    listFuncionario.Add(funcionario);
                }

                return listFuncionario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public FuncionarioVO FindById(int id)
        {
            FuncionarioVO funcionario = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Funcionarios where ID = @ID;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    funcionario = new FuncionarioVO()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        DataAdmissao = Convert.ToDateTime(Dr["DataAdmissao"]),
                        DataDesligamento = Convert.ToDateTime(Dr["DataDesligamento"]),
                        Identidade = Convert.ToString(Dr["RG"]),
                        IdTipoFuncionario = Convert.ToInt32(Dr["IDTipoFuncionario"]),
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        IdResponsavelCadastro = Convert.ToInt32(Dr["IDResponsavelCadastro"])
                    };                  
                }

                return funcionario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public FuncionarioVO FindByName(string name)
        {
            throw new NotImplementedException();
        }

    }
}
