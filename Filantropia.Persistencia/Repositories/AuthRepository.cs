﻿using Filantropia.Entidades.Utilitarios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class AuthRepository : Connection, IPersistencia<Usuario>
    {

        public Usuario FindByLoginSenha(string usuario, string senha)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Usuarios where NomeUsuario  = @id and Senha = @senha";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@id", usuario);
                Cmd.Parameters.AddWithValue("@senha", senha);
                Dr = Cmd.ExecuteReader();

                Usuario c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Usuario();

                    c.Identificador = Convert.ToInt32(Dr["ID"]);
                    c.NomeUsuario = (string)Dr["NomeUsuario"];
                    c.Senha = (string)Dr["Senha"];
                    c.Email = (string)Dr["Email"];
                    c.IdPerfil = (int)Dr["IdPerfil"];


                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


        public void Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public List<Usuario> FindAll()
        {
            throw new NotImplementedException();
        }

        public Usuario FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Usuario FindByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Insert(Usuario entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Usuario entity)
        {
            throw new NotImplementedException();
        }
    }
}
