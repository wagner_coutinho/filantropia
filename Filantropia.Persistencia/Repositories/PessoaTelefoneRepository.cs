﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class PessoaTelefoneRepository : Connection, IPersistencia<PessoaTelefone>
    {
        public void Insert(PessoaTelefone entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into PessoasTelefones (IDPessoa, IDTelefone, SMS, Whatsapp, IDTipoTelefone, DataRegistro, IDResponsavelCadastro, DataDesativacao, MotivoDesativacao, Padrao)" +
                    " values (@IDPessoa, @IDTelefone, @SMS, @Whatsapp, @IDTipoTelefone, @DataRegistro, @IDResponsavelCadastro, @DataDesativacao, @MotivoDesativacao, @Padrao)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@IDTelefone", entity.IdTelefone);
                Cmd.Parameters.AddWithValue("@SMS", entity.SMS);
                Cmd.Parameters.AddWithValue("@Whatsapp", entity.Whatsapp);
                Cmd.Parameters.AddWithValue("@IDTipoTelefone", entity.IDTipoTelefone);
                Cmd.Parameters.AddWithValue("@DataRegistro", entity.DataRegistro);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IDResponsavelCadastro);
                Cmd.Parameters.AddWithValue("@DataDesativacao", entity.DataDesativacao);
                Cmd.Parameters.AddWithValue("@MotivoDesativacao", entity.Motivo);
                Cmd.Parameters.AddWithValue("@Padrao", entity.Padrao);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(PessoaTelefone entity)
        {
            try
            {
                OpenConnection();

                string query = "update PessoasTelefones set  IDTelefone = @IDTelefone, SMS = @SMS, Whatsapp = @Whatsapp, IDTipoTelefone = @IDTipoTelefone, " +
                    "DataRegistro = @DataRegistro, IDResponsavelCadastro = @IDResponsavelCadastro, DataDesativacao = @DataDesativacao, MotivoDesativacao = @MotivoDesativacao, Padrao = @Padrao" +
                    "where IDPessoa = @IDPessoa";
                    

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@IDTelefone", entity.IdTelefone);
                Cmd.Parameters.AddWithValue("@SMS", entity.SMS);
                Cmd.Parameters.AddWithValue("@Whatsapp", entity.Whatsapp);
                Cmd.Parameters.AddWithValue("@IDTipoTelefone", entity.IDTipoTelefone);
                Cmd.Parameters.AddWithValue("@DataRegistro", entity.DataRegistro);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IDResponsavelCadastro);
                Cmd.Parameters.AddWithValue("@DataDesativacao", entity.DataDesativacao);
                Cmd.Parameters.AddWithValue("@MotivoDesativacao", entity.Motivo);
                Cmd.Parameters.AddWithValue("@Padrao", entity.Padrao);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from PessoasTelefones where IDPessoa = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<PessoaTelefone> FindAll()
        {
            List<PessoaTelefone> listPessoaTelefone = new List<PessoaTelefone>();
            PessoaTelefone pessoaTelefone = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from PessoasTelefones;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    pessoaTelefone = new PessoaTelefone()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        IdTelefone = Convert.ToInt32(Dr["IDTelefone"]),
                        SMS = Convert.ToBoolean(Dr["SMS"]),
                        Whatsapp = Convert.ToBoolean(Dr["Whatsapp"]),
                        IDTipoTelefone = Convert.ToInt32(Dr["IDTipoTelefone"]),
                        IDContato = Convert.ToInt32(Dr["IDContato"]),
                        DataRegistro = Convert.ToDateTime(Dr["DataRegistro"]),
                        IDResponsavelCadastro = Convert.ToInt32(Dr["IDResponsavelCadastro"]),
                        DataDesativacao = Convert.ToDateTime(Dr["IDTipoTelefone"]),
                        Motivo = Convert.ToString(Dr["MotivoDesativacao"]),
                        Padrao = Convert.ToBoolean(Dr["DataRegistro"])
                    };

                    listPessoaTelefone.Add(pessoaTelefone);
                }

                return listPessoaTelefone;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public PessoaTelefone FindById(int id)
        {
            PessoaTelefone pessoaTelefone = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from PessoasTelefones;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@IDPessoa",id);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    pessoaTelefone = new PessoaTelefone()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        IdTelefone = Convert.ToInt32(Dr["IDTelefone"]),
                        SMS = Convert.ToBoolean(Dr["SMS"]),
                        Whatsapp = Convert.ToBoolean(Dr["Whatsapp"]),
                        IDTipoTelefone = Convert.ToInt32(Dr["IDTipoTelefone"]),
                        IDContato = Convert.ToInt32(Dr["IDContato"]),
                        DataRegistro = Convert.ToDateTime(Dr["DataRegistro"]),
                        IDResponsavelCadastro = Convert.ToInt32(Dr["IDResponsavelCadastro"]),
                        DataDesativacao = Convert.ToDateTime(Dr["IDTipoTelefone"]),
                        Motivo = Convert.ToString(Dr["MotivoDesativacao"]),
                        Padrao = Convert.ToBoolean(Dr["DataRegistro"])
                    };

                }

                return pessoaTelefone;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public PessoaTelefone FindByName(string name)
        {
            throw new NotImplementedException();
        }

        
    }
}
