﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class PeriodicidadeRepository : Connection, IPersistencia<Periodicidade>
    {
        public void Insert(Periodicidade entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Periodicidade entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public List<Periodicidade> FindAll()
        {
            List<Periodicidade> listPeriodicidade = new List<Periodicidade>();
            Periodicidade periodicidade  = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from periodicidades;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    periodicidade = new Periodicidade()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Periodo = Convert.ToString(Dr["Periodo"]),
                        Meses = Convert.ToInt32(Dr["Meses"])
                    };

                    listPeriodicidade.Add(periodicidade);
                }

                return listPeriodicidade;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Periodicidade FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Periodicidade FindByName(string name)
        {
            throw new NotImplementedException();
        }

       
    }
}
