﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class TipoPessoaRepository : Connection, IPersistencia<TipoPessoa>
    {
        public void Insert(TipoPessoa entity)
        {
            try
            {

                OpenConnection();

                string query = "insert into TiposPessoas (Tipo, Funcionario) values(@Tipo, @Funcionario)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Tipo", entity.Tipo);
                Cmd.Parameters.AddWithValue("@Funcionario", entity.Funcionario);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(TipoPessoa entity)
        {
            try
            {

                OpenConnection();

                string query = "update TiposPessoas set Tipo = @Tipo,  Funcionario = @Funcionario where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@Tipo", entity.Tipo);
                Cmd.Parameters.AddWithValue("@Funcionario", entity.Funcionario);

                Cmd.ExecuteNonQuery();
            
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from TiposPessoas where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<TipoPessoa> FindAll()
        {
            List<TipoPessoa> listTipoPessoa = new List<TipoPessoa>();
            TipoPessoa tipoPessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from TiposPessoas;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    tipoPessoa = new TipoPessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Tipo = Convert.ToString(Dr["Tipo"]),
                        Funcionario = Convert.ToBoolean(Dr["Funcionario"])
                    };

                    listTipoPessoa.Add(tipoPessoa);
                }

                return listTipoPessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public TipoPessoa FindById(int id)
        {
            TipoPessoa tipoPessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from TiposPessoas where id = @ID;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    tipoPessoa = new TipoPessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Tipo = Convert.ToString(Dr["Tipo"]),
                        Funcionario = Convert.ToBoolean(Dr["Funcionario"])
                    };

                }

                return tipoPessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public TipoPessoa FindByName(string name)
        {
            TipoPessoa tipoPessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from TiposPessoas where Tipo = @Tipo;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@Tipo", name);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    tipoPessoa = new TipoPessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Tipo = Convert.ToString(Dr["Tipo"]),
                        Funcionario = Convert.ToBoolean(Dr["Funcionario"])
                    };

                }

                return tipoPessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
     
    }
}
