﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class PessoaEnderecoRepository : Connection, IPersistencia<PessoaEndereco>
    {
        public void Insert(PessoaEndereco entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into PessoasEnderecos (IDPessoa, IDEndereco, IDTipoEndereco, DataRegistro, IDResponsavelCadastro, Padrao)" +
                    " values (@IDPessoa, @IDEndereco, @IDTipoEndereco, @DataRegistro, @IDResponsavelCadastro, @Padrao)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@IDEndereco", entity.IdEndereco);
                Cmd.Parameters.AddWithValue("@IDTipoEndereco", entity.IdTipoEndereco);
                Cmd.Parameters.AddWithValue("@DataRegistro", entity.DataRegistro);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IDResponsavelCadastro);
                Cmd.Parameters.AddWithValue("@Padrao", entity.Padrao);


                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(PessoaEndereco entity)
        {
            try
            {
                OpenConnection();

                string query = "update PessoasEnderecos set IDEndereco = @IDEndereco, IDTipoEndereco = @IDTipoEndereco, " +
                    "DataRegistro = @DataRegistro, IDResponsavelCadastro = @IDResponsavelCadastro, Padrao = @Padrao where IDPessoa = @IDPessoa";
                    


                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@IDEndereco", entity.IdEndereco);
                Cmd.Parameters.AddWithValue("@IDTipoEndereco", entity.IdTipoEndereco);
                Cmd.Parameters.AddWithValue("@DataRegistro", entity.DataRegistro);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IDResponsavelCadastro);
                Cmd.Parameters.AddWithValue("@Padrao", entity.Padrao);


                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from PessoasEnderecos where IDPessoa = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<PessoaEndereco> FindAll()
        {
            List<PessoaEndereco> listPessoaEndereco = new List<PessoaEndereco>();
            PessoaEndereco pessoaEndereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from PessoasEnderecos;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    pessoaEndereco = new PessoaEndereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdPessoa = Convert.ToInt32(Dr["IdPessoa"]),
                        IdEndereco = Convert.ToInt32(Dr["IDEndereco"]),
                        IdTipoEndereco = Convert.ToInt32(Dr["IDTipoEndereco"]),
                        DataRegistro = Convert.ToDateTime(Dr["DataRegistro"]),
                        Padrao = Convert.ToBoolean(Dr["Padrao"])
                    };

                    listPessoaEndereco.Add(pessoaEndereco);
                }

                return listPessoaEndereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public PessoaEndereco FindById(int id)
        {
            PessoaEndereco pessoaEndereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from PessoasEnderecos where IDPessoa = @id;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    pessoaEndereco = new PessoaEndereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdPessoa = Convert.ToInt32(Dr["IdPessoa"]),
                        IdEndereco = Convert.ToInt32(Dr["IDEndereco"]),
                        IdTipoEndereco = Convert.ToInt32(Dr["IDTipoEndereco"]),
                        DataRegistro = Convert.ToDateTime(Dr["DataRegistro"]),
                        Padrao = Convert.ToBoolean(Dr["Padrao"])
                    };

                }

                return pessoaEndereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public PessoaEndereco FindByName(string name)
        {
            throw new NotImplementedException();
        }


    }
}
