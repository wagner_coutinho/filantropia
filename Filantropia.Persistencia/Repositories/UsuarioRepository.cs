﻿using System;
using System.Collections.Generic;
using Filantropia.Entidades.Utilitarios;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Filantropia.Persistencia.Repositories
{
    public class UsuarioRepository : Connection, IPersistencia<Usuario>
    {
        public void Insert(Usuario entity)
        {
            try
            {

                OpenConnection();

                string query = "insert into Usuarios (NomeUsuario, Senha, Email, IdPerfil) values (@NomeUsuario, @Senha, @Email, @IdPerfil)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@NomeUsuario", entity.NomeUsuario);
                Cmd.Parameters.AddWithValue("@Senha", entity.Senha);
                Cmd.Parameters.AddWithValue("@Email", entity.Email);
                Cmd.Parameters.AddWithValue("@IdPerfil", entity.IdPerfil);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Usuario entity)
        {
            try
            {

                OpenConnection();

                string query = "update Usuarios set NomeUsuario = @NomeUsuario,  Senha = @Senha, Email = @Email, IdPerfil = @IdPerfil  where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@NomeUsuario", entity.NomeUsuario);
                Cmd.Parameters.AddWithValue("@Senha", entity.Senha);
                Cmd.Parameters.AddWithValue("@Email", entity.Email);
                Cmd.Parameters.AddWithValue("@IdPerfil", entity.IdPerfil);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        public void Delete(int entity)
        {
            try
            {

                OpenConnection();

                string query = "delete from usuarios where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Usuario> FindAll()
        {
            List<Usuario> listUsuario = new List<Usuario>();
            Usuario usuario = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Usuarios;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    usuario = new Usuario()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        NomeUsuario = Convert.ToString(Dr["NomeUsuario"]),
                        Senha = Convert.ToString(Dr["Senha"]),
                        Email = Convert.ToString(Dr["Email"]),
                        IdPerfil = Convert.ToInt32(Dr["IdPerfil"])
                    };

                    listUsuario.Add(usuario);
                }

                return listUsuario;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Usuario FindById(int id)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Usuarios where ID = @id";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@id", id);
                Dr = Cmd.ExecuteReader();

                Usuario c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Usuario()
                    {
                        Identificador = (int)Dr["ID"],
                        NomeUsuario = (string)Dr["NomeUsuario"],
                        Senha = (string)Dr["Senha"],
                        Email = (string)Dr["Email"],
                        IdPerfil = (int)Dr["IdPerfil"]
                    };

                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Usuario FindByName(string name)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Usuarios where NomeUsuario = @nomeusuario";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@nomeusuario", name);
                Dr = Cmd.ExecuteReader();

                Usuario c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Usuario()
                    {
                        Identificador = (int)Dr["ID"],
                        NomeUsuario = (string)Dr["NomeUsuario"],
                        Senha = (string)Dr["Senha"],
                        Email = (string)Dr["Email"],
                        IdPerfil = (int)Dr["IdPerfil"]
                    };

                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
