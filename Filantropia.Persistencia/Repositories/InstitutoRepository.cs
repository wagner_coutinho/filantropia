﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class InstitutoRepository : Connection, IPersistencia<Instituto>
    {
        public void Insert(Instituto entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Institutos (Nome, CNPJ) values(@Nome, @CNPJ)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Nome", entity.Nome);
                Cmd.Parameters.AddWithValue("@CNPJ", entity.CNPJ);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Instituto entity)
        {
            try
            {
                OpenConnection();

                string query = "update Institutos set Nome = @Nome,  CNPJ = @CNPJ where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@Nome", entity.Nome);
                Cmd.Parameters.AddWithValue("@CNPJ", entity.CNPJ);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        public void Delete(int entity)
        {
            try
            {

                OpenConnection();

                string query = "delete from Institutos where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Instituto FindByName(string name)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Institutos where Nome = @nome";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@nome", name);

                Dr = Cmd.ExecuteReader();

                Instituto c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Instituto()
                    {
                        Identificador = (int)Dr["ID"],
                        Nome = (string)Dr["Nome"],
                        CNPJ = (string)Dr["CNPJ"],
                    };

                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Instituto FindById(int id)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Institutos where ID = @id";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@id", id);
                Dr = Cmd.ExecuteReader();

                Instituto c = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    c = new Instituto()
                    {
                        Identificador = (int)Dr["ID"],
                        Nome = (string)Dr["Nome"],
                        CNPJ = (string)Dr["CNPJ"],
                    };

                }
                return c;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Instituto> FindAll()
        {
            List<Instituto> listInstituto = new List<Instituto>();
            Instituto instituto = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Institutos;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    instituto = new Instituto()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Nome = Convert.ToString(Dr["Nome"]),
                        CNPJ = Convert.ToString(Dr["CNPJ"])
                    };

                    listInstituto.Add(instituto);
                }

                return listInstituto;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
