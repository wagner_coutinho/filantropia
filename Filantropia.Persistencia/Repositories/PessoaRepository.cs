﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class PessoaRepository : Connection, IPersistencia<Pessoa>
    {
        public void Insert(Pessoa entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Pessoas (Nome, IDTipoPessoa, IDResponsavelCadastro, IDEstadoCivil, CPFCNPJ) " +
                    "values(@Nome, @IDTipoPessoa, @IDResponsavelCadastro, @IDEstadoCivil, @CPFCNPJ)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Nome", entity.Nome);
                Cmd.Parameters.AddWithValue("@IDTipoPessoa", entity.IdTipoPessoa);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IdTipoPessoa);
                Cmd.Parameters.AddWithValue("@IDEstadoCivil", entity.IdEstadoCivil);
                Cmd.Parameters.AddWithValue("@CPFCNPJ", entity.CpfCnpj);


                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Pessoa entity)
        {
            try
            {
                OpenConnection();

                string query = "update Pessoas set Nome = @Nome, IDTipoPessoa = @IDTipoPessoa, IDResponsavelCadastro = @IDResponsavelCadastro, " +
                    "IDEstadoCivil = @IDEstadoCivil, CPFCNPJ = @CPFCNPJ where ID = @ID;";
                    
                Cmd = new SqlCommand(query, Con);
         
                Cmd.Parameters.AddWithValue("@Nome", entity.Nome);
                Cmd.Parameters.AddWithValue("@IDTipoPessoa", entity.IdTipoPessoa);
                Cmd.Parameters.AddWithValue("@IDResponsavelCadastro", entity.IdResponsavel);
                Cmd.Parameters.AddWithValue("@IDEstadoCivil", entity.IdEstadoCivil);
                Cmd.Parameters.AddWithValue("@CPFCNPJ", entity.CpfCnpj);
                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);

                Cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from Pessoas where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Pessoa> FindAll()
        {
            List<Pessoa> listPessoa = new List<Pessoa>();
            Pessoa pessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Pessoas;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    pessoa = new Pessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        Nome = Convert.ToString(Dr["DataAdmissao"]),
                        IdTipoPessoa = Convert.ToInt32(Dr["DataDesligamento"]),
                        IdResponsavel = Convert.ToInt32(Dr["RG"]),
                        IdEstadoCivil = Convert.ToInt32(Dr["IDTipoFuncionario"]),
                        CpfCnpj = Convert.ToString(Dr["IDPessoa"]),                       
                    };

                    listPessoa.Add(pessoa);
                }

                return listPessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Pessoa FindById(int id)
        {
            Pessoa pessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Pessoas where id = @id;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                if(Dr.Read())
                {
                    pessoa = new Pessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        Nome = Convert.ToString(Dr["DataAdmissao"]),
                        IdTipoPessoa = Convert.ToInt32(Dr["DataDesligamento"]),
                        IdResponsavel = Convert.ToInt32(Dr["RG"]),
                        IdEstadoCivil = Convert.ToInt32(Dr["IDTipoFuncionario"]),
                        CpfCnpj = Convert.ToString(Dr["IDPessoa"]),
                    };

                }

                return pessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Pessoa FindByName(string name, int tipoPessoa)
        {
            Pessoa pessoa = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Pessoas where nome = @nome and IDTipoPessoa in (10012, 12, 1);";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@nome", name);
                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    pessoa = new Pessoa()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),

                        Nome = Convert.ToString(Dr["DataAdmissao"]),
                        IdTipoPessoa = Convert.ToInt32(Dr["DataDesligamento"]),
                        IdResponsavel = Convert.ToInt32(Dr["RG"]),
                        IdEstadoCivil = Convert.ToInt32(Dr["IDTipoFuncionario"]),
                        CpfCnpj = Convert.ToString(Dr["IDPessoa"]),
                    };

                }

                return pessoa;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        
    }
}
