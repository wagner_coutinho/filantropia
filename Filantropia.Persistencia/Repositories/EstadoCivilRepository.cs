﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class EstadoCivilRepository : Connection, IPersistencia<EstadoCivil>
    {
        public void Insert(EstadoCivil entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into EstadosCivis (@EstadoCivil)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Banco", entity.Descricao);


                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(EstadoCivil entity)
        {
            try
            {
                OpenConnection();

                string query = "update EstadosCivis set EstadoCivil = @EstadoCivil where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@EstadoCivil", entity.Descricao);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from EstadosCivis where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<EstadoCivil> FindAll()
        {
            List<EstadoCivil> listEstadoCivil = new List<EstadoCivil>();
            EstadoCivil estadoCivil = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from EstadosCivis;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    estadoCivil = new EstadoCivil()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Descricao = Convert.ToString(Dr["EstadoCivil"]),
                    };

                    listEstadoCivil.Add(estadoCivil);
                }

                return listEstadoCivil;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public EstadoCivil FindById(int id)
        {
            EstadoCivil estadoCivil = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from EstadosCivis where ID = @ID;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);

                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    estadoCivil = new EstadoCivil()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Descricao = Convert.ToString(Dr["EstadoCivil"]),
                    };

                }

                return estadoCivil;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public EstadoCivil FindByName(string name)
        {
            EstadoCivil estadoCivil = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from EstadosCivis where EstadoCivil = @EstadoCivil;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@EstadoCivil", name);

                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    estadoCivil = new EstadoCivil()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Descricao = Convert.ToString(Dr["EstadoCivil"]),
                    };

                }

                return estadoCivil;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
