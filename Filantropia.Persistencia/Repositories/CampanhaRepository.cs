﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class CampanhaRepository : Connection, IPersistencia<Campanha>
    {
        public void Insert(Campanha entity)
        {
            try
            {

                OpenConnection();

                string query = "insert into Campanhas (IDInstituto ,Campanha, Descricao, Inicio, Fim) values (@IDInstituto, @Campanha, @Descricao, @Inicio, @Fim)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDInstituto", entity.IdInstituto);
                Cmd.Parameters.AddWithValue("@Campanha", entity.NomeCampanha);
                Cmd.Parameters.AddWithValue("@Descricao", entity.Descricao);
                Cmd.Parameters.AddWithValue("@Inicio", entity.Inicio);
                Cmd.Parameters.AddWithValue("@Fim", entity.Fim);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Campanha entity)
        {
            try
            {

                OpenConnection();

                string query = "update Campanhas set IdInstituto = @IDInstituto, Campanha = @Campanha,  Descricao = @Descricao, Inicio = @Inicio, Fim = @Fim where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@IDInstituto", entity.IdInstituto);
                Cmd.Parameters.AddWithValue("@Campanha", entity.NomeCampanha);
                Cmd.Parameters.AddWithValue("@Descricao", entity.Descricao);
                Cmd.Parameters.AddWithValue("@Inicio", entity.Inicio);
                Cmd.Parameters.AddWithValue("@Fim", entity.Fim);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }
        public void Delete(int entity)
        {
            try
            {

                OpenConnection();

                string query = "delete from Campanhas where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Campanha> FindAll()
        {
            List<Campanha> listCampanha = new List<Campanha>();
            Campanha campanha = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Campanhas;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    campanha = new Campanha()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdInstituto = Convert.ToInt32(Dr["IdInstituto"]),
                        NomeCampanha = Convert.ToString(Dr["Campanha"]),
                        Descricao = Convert.ToString(Dr["Descricao"]),
                        Inicio = Convert.ToDateTime(Dr["Inicio"]),
                        Fim = Convert.ToDateTime(Dr["Fim"])
                    };

                    listCampanha.Add(campanha);
                }

                return listCampanha;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Campanha FindById(int id)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Campanhas where ID = @id";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@id", id);
                Dr = Cmd.ExecuteReader();

                Campanha campanha = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    campanha = new Campanha()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdInstituto = Convert.ToInt32(Dr["IdInstituto"]),
                        NomeCampanha = Convert.ToString(Dr["Campanha"]),
                        Descricao = Convert.ToString(Dr["Descricao"]),
                        Inicio = Convert.ToDateTime(Dr["Inicio"]),
                        Fim = Convert.ToDateTime(Dr["Fim"])
                    };

                }
                return campanha;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Campanha FindByName(string name)
        {
            try
            {
                OpenConnection();

                //conectar na base de dados..
                string query = "select * from Campanhas where Campanha = @name";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@name", name);
                Dr = Cmd.ExecuteReader();

                Campanha campanha = null;

                if (Dr.Read()) //se foi encontrado um registro..
                {
                    campanha = new Campanha()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdInstituto = Convert.ToInt32(Dr["IdInstituto"]),
                        NomeCampanha = Convert.ToString(Dr["Campanha"]),
                        Descricao = Convert.ToString(Dr["Descricao"]),
                        Inicio = Convert.ToDateTime(Dr["Inicio"]),
                        Fim = Convert.ToDateTime(Dr["Fim"])
                    };

                }
                return campanha;
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
