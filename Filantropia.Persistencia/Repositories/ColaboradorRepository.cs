﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Entidades.VO;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class ColaboradorRepository : Connection, IPersistencia<ColaboradorVO>
    {
        public void Insert(ColaboradorVO entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Colaboradores (IDPessoa, DataInativacao, DiaContribuicao, ValorContribuicao, IDPeriodicidade, IDChamadaOriginaria, UltimaContribuicao, DataCadastro) " +
                    "values(@IDPessoa, @DataInativacao, @DiaContribuicao, @ValorContribuicao, @IDPeriodicidade, @IDChamadaOriginaria, @UltimaContribuicao, CAST(GETDATE() AS DATE))";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", entity.IdPessoa);
                Cmd.Parameters.AddWithValue("@DataInativacao", entity.DataInativacao);
                Cmd.Parameters.AddWithValue("@DiaContribuicao", entity.DiaContribuicao);
                Cmd.Parameters.AddWithValue("@ValorContribuicao", entity.ValorContribuicao);
                Cmd.Parameters.AddWithValue("@IDPeriodicidade", entity.IdPeriodicidade);
                Cmd.Parameters.AddWithValue("@IDChamadaOriginaria", entity.IdChamadaOriginaria);
                Cmd.Parameters.AddWithValue("@UltimaContribuicao", entity.UltimaContribuicao);
                
                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(ColaboradorVO entity)
        {
            try
            {
                OpenConnection();

                string query = "update colaboradores set DataInativacao = @DataInativacao, ValorContribuicao = @ValorContribuicao, IDPeriodicidade = @IDPeriodicidade, IDChamadaOriginaria = @IDChamadaOriginaria," +
                                "UltimaContribuicao = @UltimaContribuicao, DataCadastro = @DataCadastro where IDPessoa = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);
                Cmd.Parameters.AddWithValue("@DataInativacao", entity.DataInativacao);
                Cmd.Parameters.AddWithValue("@ValorContribuicao", entity.ValorContribuicao);
                Cmd.Parameters.AddWithValue("@IDPeriodicidade", entity.IdPeriodicidade);
                Cmd.Parameters.AddWithValue("@IDChamadaOriginaria", entity.IdChamadaOriginaria);
                Cmd.Parameters.AddWithValue("@UltimaContribuicao", entity.UltimaContribuicao);
                Cmd.Parameters.AddWithValue("@DataCadastro", entity.DataCadastro);

                Cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from colaboradores where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<ColaboradorVO> FindAll()
        {
            List<ColaboradorVO> listColaborador = new List<ColaboradorVO>();
            ColaboradorVO colaborador = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query =  " select a.IDPessoa, b.Nome, a.DiaContribuicao, a.ValorContribuicao, c.Periodo,  b.CPFCNPJ, a.DataCadastro " +
                                " from Colaboradores a "+
                                " join Pessoas b on (a.IDPessoa = b.ID) "+
                                " join Periodicidades c on (a.IDPeriodicidade = c.ID) "+
                                " where b.IDTipoPessoa = 20012; ";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    colaborador = new ColaboradorVO()
                    {
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        Nome = Convert.ToString(Dr["Nome"]),
                        DiaContribuicao = Convert.ToInt32(Dr["DiaContribuicao"]),
                        ValorContribuicao = Convert.ToDecimal(Dr["ValorContribuicao"]),
                        Periodo = Convert.ToString(Dr["Periodo"]),
                        CpfCnpj = Convert.ToString(Dr["CPFCNPJ"]),
                        DataCadastro = Convert.ToDateTime(Dr["DataCadastro"])
                    };

                    listColaborador.Add(colaborador);
                }

                return listColaborador;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public ColaboradorVO FindById(int id)
        {
            ColaboradorVO colaborador = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = " select a.IDPessoa, b.Nome, a.DiaContribuicao, a.ValorContribuicao, c.Periodo,  b.CPFCNPJ, a.DataCadastro " +
                               " from Colaboradores a " +
                               " join Pessoas b on (a.IDPessoa = b.ID) " +
                               " join Periodicidades c on (a.IDPeriodicidade = c.ID) " +
                               " where b.IDTipoPessoa = 20012 and a.IDPessoa = @IDPessoa; ";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@IDPessoa", id);
                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    colaborador = new ColaboradorVO()
                    {
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        Nome = Convert.ToString(Dr["Nome"]),
                        DiaContribuicao = Convert.ToInt32(Dr["DiaContribuicao"]),
                        ValorContribuicao = Convert.ToDecimal(Dr["ValorContribuicao"]),
                        Periodo = Convert.ToString(Dr["Periodo"]),
                        CpfCnpj = Convert.ToString(Dr["CPFCNPJ"]),
                        DataCadastro = Convert.ToDateTime(Dr["DataCadastro"])
                    };

                }

                return colaborador;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public ColaboradorVO FindByName(string name)
        {
            ColaboradorVO colaborador = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Colaboradores where Nome = @nome;";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@nome", name);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    colaborador = new ColaboradorVO()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        IdPessoa = Convert.ToInt32(Dr["IDPessoa"]),
                        DataInativacao = Convert.ToDateTime(Dr["DataInativacao"]),
                        DiaContribuicao = Convert.ToInt32(Dr["DiaContribuicao"]),
                        IdPeriodicidade = Convert.ToInt32(Dr["IDPeriodicidade"]),
                        IdChamadaOriginaria = Convert.ToInt32(Dr["IDChamadaOriginaria"]),
                        UltimaContribuicao = Convert.ToDateTime(Dr["UltimaContribuicao"]),
                        DataCadastro = Convert.ToDateTime(Dr["DataCadastro"])
                    };

                }

                return colaborador;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
