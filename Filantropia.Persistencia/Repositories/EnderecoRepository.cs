﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia.Repositories
{
    public class EnderecoRepository : Connection, IPersistencia<Endereco>
    {
        public void Insert(Endereco entity)
        {
            try
            {
                OpenConnection();

                string query = "insert into Enderecos (Logadouro, Numero, Complemento, Bairro, Cidade, UF, CEP) " +
                    "values(@Logadouro, @Numero, @Complemento, @Bairro, @Cidade, @UF, @CEP)";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Logadouro", entity.Logadouro);
                Cmd.Parameters.AddWithValue("@Numero", entity.Numero);
                Cmd.Parameters.AddWithValue("@Complemento", entity.Complemento);
                Cmd.Parameters.AddWithValue("@Bairro", entity.Bairro);
                Cmd.Parameters.AddWithValue("@Cidade", entity.Cidade);
                Cmd.Parameters.AddWithValue("@UF", entity.UF);
                Cmd.Parameters.AddWithValue("@CEP", entity.CEP);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Update(Endereco entity)
        {
            try
            {
                OpenConnection();

                string query = "update Enderecos set Logradouro = @Logadouro," +
                    " Numero = @Numero, " +
                    " Complemento = @Complemento, " +
                    " Bairro = @Bairro, " +
                    " Cidade = @Cidade, " +
                    " UF = @Uf, " +
                    " CEP = @CEP " +
                    " where ID = @id;";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@Logadouro", entity.Logadouro);
                Cmd.Parameters.AddWithValue("@Numero", entity.Numero);
                Cmd.Parameters.AddWithValue("@Complemento", entity.Complemento);
                Cmd.Parameters.AddWithValue("@Bairro", entity.Bairro);
                Cmd.Parameters.AddWithValue("@Cidade", entity.Cidade);
                Cmd.Parameters.AddWithValue("@UF", entity.UF);
                Cmd.Parameters.AddWithValue("@CEP", entity.CEP);
                Cmd.Parameters.AddWithValue("@ID", entity.Identificador);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public void Delete(int entity)
        {
            try
            {
                OpenConnection();

                string query = "delete from Enderecos where ID = @ID";

                Cmd = new SqlCommand(query, Con);

                Cmd.Parameters.AddWithValue("@ID", entity);

                Cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Erro: " + e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Endereco> FindAll()
        {
            List<Endereco> listEndereco = new List<Endereco>();
            Endereco endereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Enderecos;";

                Cmd = new SqlCommand(query, Con);

                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    endereco = new Endereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Logadouro = Convert.ToString(Dr["Logadouro"]),
                        Numero = Convert.ToInt32(Dr["Numero"]),
                        Complemento = Convert.ToString(Dr["Complemento"]),
                        Bairro = Convert.ToString(Dr["Bairro"]),
                        Cidade = Convert.ToString(Dr["Cidade"]),
                        UF = Convert.ToString(Dr["UF"]),
                        CEP = Convert.ToString(Dr["CEP"])
                    };

                    listEndereco.Add(endereco);
                }

                return listEndereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Endereco FindById(int id)
        {
            Endereco endereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Enderecos where ID = @ID;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@ID", id);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    endereco = new Endereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Logadouro = Convert.ToString(Dr["Logadouro"]),
                        Numero = Convert.ToInt32(Dr["Numero"]),
                        Complemento = Convert.ToString(Dr["Complemento"]),
                        Bairro = Convert.ToString(Dr["Bairro"]),
                        Cidade = Convert.ToString(Dr["Cidade"]),
                        UF = Convert.ToString(Dr["UF"]),
                        CEP = Convert.ToString(Dr["CEP"])
                    };

                }

                return endereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Endereco FindByName(string name)
        {
            Endereco endereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Enderecos where Logadouro = @Logadouro;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@Logadouro", name);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    endereco = new Endereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Logadouro = Convert.ToString(Dr["Logadouro"]),
                        Numero = Convert.ToInt32(Dr["Numero"]),
                        Complemento = Convert.ToString(Dr["Complemento"]),
                        Bairro = Convert.ToString(Dr["Bairro"]),
                        Cidade = Convert.ToString(Dr["Cidade"]),
                        UF = Convert.ToString(Dr["UF"]),
                        CEP = Convert.ToString(Dr["CEP"])
                    };

                }

                return endereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Endereco FindByIdPessoa(int id)
        {
            Endereco endereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = " select e.Logradouro, e.CEP, e.Bairro, e.Numero, e.Cidade, e.Complemento " +
                               " from Enderecos e " +
                               " join PessoasEnderecos d on (e.ID = d.IDEndereco) " +
                               " where d.IDPessoa = @IDPessoa ";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@IDPessoa", id);
                Dr = Cmd.ExecuteReader();

                if (Dr.Read())
                {
                    endereco = new Endereco()
                    {
                        Logadouro = Convert.ToString(Dr["Logradouro"]),
                        Numero = Convert.ToInt32(Dr["Numero"]),
                        Complemento = Convert.ToString(Dr["Complemento"]),
                        Bairro = Convert.ToString(Dr["Bairro"]),
                        Cidade = Convert.ToString(Dr["Cidade"]),
                        CEP = Convert.ToString(Dr["CEP"])
                    };

                }

                return endereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        public Endereco FindByEnderecoAndNumeroAndComplemento(string name, int numero, string complemento)
        {
            Endereco endereco = null;

            try
            {
                OpenConnection(); //abrir conexão..

                string query = "select * from Enderecos where Logadouro = @Logadouro and Numero = @Numero and Complemento = @Complemento;";

                Cmd = new SqlCommand(query, Con);
                Cmd.Parameters.AddWithValue("@Logadouro", name);
                Cmd.Parameters.AddWithValue("@Numero", numero);
                Cmd.Parameters.AddWithValue("@Complemento", complemento);
                Dr = Cmd.ExecuteReader();

                while (Dr.Read())
                {
                    endereco = new Endereco()
                    {
                        Identificador = Convert.ToInt32(Dr["ID"]),
                        Logadouro = Convert.ToString(Dr["Logadouro"]),
                        Numero = Convert.ToInt32(Dr["Numero"]),
                        Complemento = Convert.ToString(Dr["Complemento"]),
                        Bairro = Convert.ToString(Dr["Bairro"]),
                        Cidade = Convert.ToString(Dr["Cidade"]),
                        UF = Convert.ToString(Dr["UF"]),
                        CEP = Convert.ToString(Dr["CEP"])
                    };

                }

                return endereco;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                CloseConnection();
            }
        }


    }
}
