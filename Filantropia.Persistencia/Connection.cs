﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia
{
    public class Connection
    {
        protected SqlConnection Con; 
        protected SqlCommand Cmd; 
        protected SqlDataReader Dr; 
        protected SqlTransaction Tr;

        protected void OpenConnection() 
        { 
            Con = new SqlConnection(ConfigurationManager.ConnectionStrings["conexao"].ConnectionString);            
            Con.Open();
        }

        protected void CloseConnection()
        {
            if (Con != null) Con.Close(); 
        }
    }
}
