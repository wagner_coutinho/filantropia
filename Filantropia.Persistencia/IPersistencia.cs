﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Persistencia
{
    public interface IPersistencia<T> where T : class
    {
        void Insert(T entity); 

        void Update(T entity);

        void Delete(int entity);
        
        T FindByName(string name);

        T FindById(int id);

        List<T> FindAll();

    }
}
