﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Util.Seguranca
{
    public class Criptografia
    {
        //método estático para encriptar um valor para MD5
        public static string EncriptarMD5(string valor) 
        {
            MD5 md5 = new MD5CryptoServiceProvider(); 
            byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(valor)); 
            
            return BitConverter.ToString(hash).Replace("-", string.Empty); 
        
        }

        //método estático para encriptar um valor para SHA1
        public static string EncriptarSHA1(string valor) 
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider(); 
            byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(valor)); 
            
            return BitConverter.ToString(hash).Replace("-", string.Empty); 
        
        }
    }
}
