﻿function Login() {


    var model = {

        NomeUsuario: $("#nomeUsuario").val(),
        Senha: $("#senha").val(),

    };


    $.ajax({
        type: "POST",
        url: "/Auth/Login",
        data: model,
        success: function (msg) {
            //exibir mensagem 

            console.log(msg);

            if (msg == "Usuario ou senha invalidos.") {

                $("#mensagem").html(msg);

            }


            if (msg == "Dashboard/Dashboard") {

                window.location.href = "Dashboard/Dashboard";
            }


        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });




};


function Submit() {

    var model = {

        NomeUsuario: $("#nomeUsuario").val(),
        Senha: $("#senha").val(),

    };

    if (model.NomeUsuario != "" && model.Senha != "") {

        $("#btnLogin").prop("disabled", false);

    }

    if (model.NomeUsuario == "" && model.Senha != "") {

        $("#btnLogin").prop("disabled", true);

    }

    if (model.NomeUsuario != "" && model.Senha == "") {

        $("#btnLogin").prop("disabled", true);

    }

};

function GetAll() {
    $.ajax({
        type: "GET",
        url: "/Usuario/ListarTodos",
        data: {},
        success: function (lista) {

            var conteudo = "";
            $.each(lista, function (i, p) {
                conteudo += "<tr>";
                conteudo += "<td>" + p.identificador + "</td>";
                conteudo += "<td colspan='5'>" + p.nomeUsuario + "</td>";
                conteudo += "<td></td>";
                conteudo += "<td></td>";
                conteudo += "<td><div class='btn-group'><button type='button' class='btn  btn-sm btn-primary fa fa-check'></button><button type='button' class='btn  btn-sm btn-danger fa fa-close'></button></div></td>";
                conteudo += "</tr>";

            })
            $("#tabelaUsuario tbody").html(conteudo);

        },
        error: function (e) {
            Mensagem(e.status, 1);
        }

    });

}

window.onload = function () {
    Submit();
}