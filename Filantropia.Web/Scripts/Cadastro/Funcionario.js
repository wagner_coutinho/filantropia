﻿function Salvar() {

    var model = {

        nome: $("#nomeFuncionario").val(),
        cpfcnpj: $("#cpfcnpjFuncionario").val(),
        tipoPessoa: $("#selTipoPessoa").val(),
        dataAdmissao: $("#dtAdmissao").val(),
        dataDesligamento: $("#dtDemissao").val(),
        ativo: $("#selAtivo").val(),
        senha: $("#txtSenha").val(),
        cep: $("#cepFuncionario").val(),
        logadouro: $("#enderecoFuncionario").val(),
        numero: $("#numeroFuncionario").val(),
        complemento: $("#complementoFuncionario").val(),
        bairro: $("#bairroFuncionario").val(),
        cidade: $("#municipioFuncionario").val(),
        telefoneFixo: $("#telefoneFixo").val(),
        telefoneCelular: $("#telefoneCelular").val(),
        telefoneAlternativo: $("#telefoneAlternativo").val(),
        chavePix: $("#chavePix").val(),
        idEstadoCivil: $("#selEstadoCivil").val(),
    };

    $.ajax({
        type: "POST",
        url: "/Funcionario/Post",
        data: model,
        success: function (msg) {
            //exibir mensagem 
            $("#mensagem").html(msg);

            //limpar os campos.. 
            $(".form-control").val("");

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });


};

function GetCep() {


    var cep = $("#cepFuncionario").val();
    var url = "https://viacep.com.br/ws/" + cep + "/json/"

    $.ajax({
        type: "GET",
        url: url,
        data: {},
        success: function (data) {

            $("#enderecoFuncionario").val(data.logradouro);
            $("#bairroFuncionario").val(data.bairro);
            $("#municipioFuncionario").val(data.localidade);
            $("#complementoFuncionario").val(data.complemento)

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });
}



function GetById(idFuncionario) {

    $('#modal-Funcionario').modal('show');

    $.ajax({
        type: "GET",
        url: "/Funcionario/GetDoador/" + idFuncionario,
        success: function (result) {

            //console.log(result);

            if (result != "") {

                $("#nomeFuncionario").val(result.Nome);
                $("#cpfFuncionario").val(result.CpfCnpj);
                $("#cepFuncionario").val(result.CEP);
                $("#enderecoFuncionario").val(result.Logadouro);
                $("#complementoFuncionario").val(result.Complemento);
                $("#numeroFuncionario").val(result.Numero);
                $("#bairroFuncionario").val(result.Bairro);
                $("#municipioFuncionario").val(result.Cidade);
                $("#valorContribuicaoFuncionario").val(result.ValorContribuicao);
                $("#diaContribuicaoFuncionario").val(result.DiaContribuicao);
                $("#selPeriodicidade").val(result.Periodo);



            }
        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });


};




function GetAll() {
    $.ajax({
        type: "GET",
        url: "/Funcionario/ListarTodos",
        data: {},
        success: function (lista) {

            var conteudo = "";
            $.each(lista, function (i, p) {
                conteudo += "<tr>";
                conteudo += "<td>" + p.idPessoa + "</td>";
                conteudo += "<td colspan='5'>" + p.nome + "</td>";
                conteudo += "<td>" + p.periodo + "</td>";
                conteudo += "<td>" + p.cpfCnpj + "</td>";
                conteudo += "<td><div class='btn-group'><button type='button' class='btn  btn-sm btn-primary fa fa-check'onclick='GetById(" + p.idPessoa + ")'></button><button type='button' class='btn  btn-sm btn-danger fa fa-close'></button></div></td>";
                conteudo += "</tr>";

            })
            $("#tabelaFuncionario tbody").html(conteudo);

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });

}

function GetTipoFuncionario() {

    $.ajax({
        type: "GET",
        url: "/TipoPessoa/ListarTodos",
        async: false,
        success: function (lista) {
            $.each(lista, function (i, p) {

                $("#selTipoPessoa").append($("<option>", {

                    value: lista[i].identificador,
                    text: lista[i].tipo

                },
                    "</option>"))

            });
        }
    });
};

function GetTipoFuncionario() {

    $.ajax({
        type: "GET",
        url: "/TipoPessoa/ListarTodos",
        async: false,
        success: function (lista) {
            $.each(lista, function (i, p) {

                $("#selTipoPessoa").append($("<option>", {

                    value: lista[i].identificador,
                    text: lista[i].tipo

                },
                    "</option>"))

            });
        }
    });
};

//function tabelaTelefones() {

//    // Start counting from the third row
//    var counter = 1;

//    $("#insertRow").on("click", function (event) {
//        event.preventDefault();

//        var newRow = $("<tr>");
//        var cols = '';

//        // Table columns
//        cols += '<th scrope="row">' + counter + '</th>';
//        cols += '<td><input class="form-control rounded-0" type="text" name="firstname" id="telfuncionario" placeholder="First name"></td>';
//        cols += '<td><input class="form-control rounded-0" type="text" name="lastname" id="contatofuncionario" placeholder="Last name"></td>';
//        cols += '<td><button class="btn btn-danger rounded-0" id ="deleteRow"><i class="fa fa-trash"></i></button</td>';

//        // Insert the columns inside a row
//        newRow.append(cols);

//        // Insert the row inside a table
//        $("table").append(newRow);

//        // Increase counter after each row insertion
//        counter++;
//    });

//    // Remove row when delete btn is clicked
//    $("table").on("click", "#deleteRow", function (event) {
//        $(this).closest("tr").remove();
//        counter -= 1
//    });
//};

window.onload = function () {
    //$(function () {

    //    // Start counting from the third row
    //    var counter = 1;

    //    $("#insertRow").on("click", function (event) {
    //        event.preventDefault();

    //        var newRow = $("<tr>");
    //        var cols = '';

    //        // Table columns
    //        cols += '<th scrope="row">' + counter + '</th>';
    //        cols += '<td><input class="form-control rounded-0" type="text" name="firstname" placeholder="Telefone"></td>';
    //        cols += '<td><input class="form-control rounded-0" type="text" name="lastname" placeholder="Nome de Contato"></td>';
    //        cols += '<td><button class="btn btn-danger rounded-0" id ="deleteRow"><i class="fa fa-trash"></i></button</td>';

    //        // Insert the columns inside a row
    //        newRow.append(cols);

    //        // Insert the row inside a table
    //        $("#table").append(newRow);

    //        // Increase counter after each row insertion
    //        counter++;
    //    });

    //    // Remove row when delete btn is clicked
    //    $("#table").on("click", "#deleteRow", function (event) {
    //        $(this).closest("tr").remove();
    //        counter -= 1
    //    });
    //});

    GetAll();
    GetTipoFuncionario();
}