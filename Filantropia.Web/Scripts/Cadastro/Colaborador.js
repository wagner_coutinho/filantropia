﻿function Salvar() {

    var model = {

        nomeBanco: $("#nomeBanco").val(),
        codigoBanco: $("#codigoBanco").val(),
        agencia: $("#agenciaBanco").val(),
        conta: $("#categoria").val(),
        chavePix: $("#chavePix").val()
    };

    $.ajax({
        type: "POST",
        url: "/Banco/Post",
        data: model,
        success: function (msg) {
            //exibir mensagem 
            $("#mensagem").html(msg);

            //limpar os campos.. 
            $(".form-control").val("");

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });


};


function GetCep() {

    
    var cep = $("#cepColaborador").val();
    var url = "https://viacep.com.br/ws/" + cep + "/json/"

    $.ajax({
        type: "GET",
        url: url,
        data: {},
        success: function (data) {

            $("#enderecoColaborador").val(data.logradouro);
            $("#bairroColaborador").val(data.bairro);
            $("#municipioColaborador").val(data.localidade);
            $("#complementoColaborador").val(data.complemento)

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });
}



function GetById(idColaborador) {

    $('#modal-colaborador').modal('show');

    $.ajax({
        type: "GET",
        url: "/Colaborador/GetDoador/" + idColaborador,
        success: function (result) {

            //console.log(result);

            if (result != "") {

                $("#nomeColaborador").val(result.Nome);
                $("#cpfColaborador").val(result.CpfCnpj);
                $("#cepColaborador").val(result.CEP);
                $("#enderecoColaborador").val(result.Logadouro);
                $("#complementoColaborador").val(result.Complemento);
                $("#numeroColaborador").val(result.Numero);
                $("#bairroColaborador").val(result.Bairro);
                $("#municipioColaborador").val(result.Cidade);
                $("#valorContribuicaoColaborador").val(result.ValorContribuicao);
                $("#diaContribuicaoColaborador").val(result.DiaContribuicao);
                $("#selPeriodicidade").val(result.Periodo);



            }
        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });


};




function GetAll() {
    $.ajax({
        type: "GET",
        url: "/Colaborador/ListarTodos",
        data: {},
        success: function (lista) {

            var conteudo = "";
            $.each(lista, function (i, p) {
                conteudo += "<tr>";
                conteudo += "<td>" + p.idPessoa + "</td>";
                conteudo += "<td colspan='5'>" + p.nome + "</td>";
                conteudo += "<td>" + p.periodo + "</td>";
                conteudo += "<td>" + p.cpfCnpj + "</td>";
                conteudo += "<td><div class='btn-group'><button type='button' class='btn  btn-sm btn-primary fa fa-check'onclick='GetById(" + p.idPessoa + ")'></button><button type='button' class='btn  btn-sm btn-danger fa fa-close'></button></div></td>";
                conteudo += "</tr>";

            })
            $("#tabelaColaborador tbody").html(conteudo);

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });

}

function GetPeriodicidade() {

    $.ajax({
        type: "GET",
        url: "/Periodicidade/ListarTodos",
        async: false,
        success: function (lista) {
            $.each(lista, function (i, p) {

                $("#selPeriodicidade").append($("<option>", {

                    value: lista[i].identificador,
                    text: lista[i].periodo

                },
                "</option>"))

            });
        }
    });
};

window.onload = function () {
    $(function () {

        // Start counting from the third row
        var counter = 1;

        $("#insertRow").on("click", function (event) {
            event.preventDefault();

            var newRow = $("<tr>");
            var cols = '';

            // Table columns
            cols += '<th scrope="row">' + counter + '</th>';
            cols += '<td><input class="form-control rounded-0" type="text" name="firstname" placeholder="Telefone"></td>';
            cols += '<td><input class="form-control rounded-0" type="text" name="lastname" placeholder="Nome de Contato"></td>';
            cols += '<td><button class="btn btn-danger rounded-0" id ="deleteRow"><i class="fa fa-trash"></i></button</td>';

            // Insert the columns inside a row
            newRow.append(cols);

            // Insert the row inside a table
            $("#table").append(newRow);

            // Increase counter after each row insertion
            counter++;
        });

        // Remove row when delete btn is clicked
        $("table").on("click", "#deleteRow", function (event) {
            $(this).closest("tr").remove();
            counter -= 1
        });
    });

    GetAll();
    GetPeriodicidade();
}