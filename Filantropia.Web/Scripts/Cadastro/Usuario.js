﻿
function Salvar() {

    var model = {

        nomeUsuario: $("#nomeUsuario").val(),
        senha: $("#senha").val(),
        email: $("#email").val(),
        idPerfil: $("#idPerfil option:selected").val()

    };

    $.ajax({
        type: "POST",
        url: "/Usuario/Post",
        data: model,
        success: function (msg) {
            //exibir mensagem 
            $("#mensagem").html(msg);

            //limpar os campos.. 
            $(".form-control").val("");

        },
        error: function (e) {
            $("#mensagem").html("Erro: " + e.status);
        }

    });


};

function GetAll() {
    $.ajax({
        type: "GET",
        url: "/Usuario/ListarTodos",
        data: {},
        success: function (lista) {

            var conteudo = "";
            $.each(lista, function (i, p) {
                conteudo += "<tr>";
                conteudo += "<td>" + p.identificador + "</td>";
                conteudo += "<td colspan='5'>" + p.nomeUsuario + "</td>";
                conteudo += "<td></td>";
                conteudo += "<td></td>";
                conteudo += "<td><div class='btn-group'><button type='button' class='btn  btn-sm btn-primary fa fa-check'></button><button type='button' class='btn  btn-sm btn-danger fa fa-close'></button></div></td>";
                conteudo += "</tr>";

            })
            $("#tabelaUsuario tbody").html(conteudo);

        },
        error: function (e) {
            Mensagem(e.status, 1);
        }

    });

}

window.onload = function () {
    GetAll();
}