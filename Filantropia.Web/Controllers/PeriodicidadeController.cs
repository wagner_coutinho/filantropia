﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class PeriodicidadeController : Controller, IController<PeriodicidadeViewModel>
    {
        // GET: Periodicidade
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public PeriodicidadeViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public JsonResult ListarTodos()
        {
            try
            {
                PeriodicidadeService periodicidadeService  = new PeriodicidadeService();

                IList<PeriodicidadeViewModel> lista = new List<PeriodicidadeViewModel>();

                foreach (var item in periodicidadeService.FindAll())
                {
                    PeriodicidadeViewModel c = new PeriodicidadeViewModel();

                    c.identificador = item.Identificador;
                    c.periodo = item.Periodo;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult Post(PeriodicidadeViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}