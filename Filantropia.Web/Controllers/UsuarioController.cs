﻿using Filantropia.ClassesNegocio.Utilitarios;
using Filantropia.Entidades.Utilitarios;
using Filantropia.Util.Seguranca;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class UsuarioController : Controller, IController<UsuarioViewModel>
    {
        // GET: Usuario
        public ActionResult Usuario()
        {
            return View();
        }
        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public UsuarioViewModel Get(int id)
        {
            try
            {
                UsuarioViewModel usuarioViewModel = new UsuarioViewModel();
                UsuarioService usuarioService = new UsuarioService();

                Usuario usuario = new Usuario();

                usuario = usuarioService.FindById(id);

                if(usuario != null)
                {
                    UsuarioViewModel c = new UsuarioViewModel();

                    usuarioViewModel.identificador = usuario.Identificador;
                    usuarioViewModel.nomeUsuario = usuario.NomeUsuario;
                    usuarioViewModel.senha = usuario.Senha;
                    usuarioViewModel.email = usuario.Email;
                    usuarioViewModel.idPerfil = usuario.IdPerfil;


                }

                return usuarioViewModel;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public JsonResult ListarTodos()
        {
            try
            {
                UsuarioService usuarioService = new UsuarioService();

                IList<UsuarioViewModel> lista = new List<UsuarioViewModel>();

                foreach (var item in usuarioService.FindAll())
                {
                    UsuarioViewModel c = new UsuarioViewModel();

                    c.identificador = item.Identificador;
                    c.nomeUsuario = item.NomeUsuario;
                    c.senha = item.Senha;
                    c.email = item.Email;
                    c.idPerfil = item.IdPerfil;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult Post(UsuarioViewModel entity)
        {
            Usuario usuarioBase = null;

            UsuarioService usuarioService  = new UsuarioService();
            try
            {
                usuarioBase = new Usuario()
                {
                    Identificador = entity.identificador,
                    NomeUsuario = entity.nomeUsuario,
                    Senha = Criptografia.EncriptarMD5(entity.senha),
                    Email = entity.email,
                    IdPerfil = entity.idPerfil
                };

                usuarioService.Save(usuarioBase);

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);

            }

            return Json("Cadastrado com sucesso.");
        }
       
    }
}
