﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class ColaboradorController : Controller, IController<ColaboradorViewModel>
    {
        // GET: Colaborador
        public ActionResult Colaborador()
        {
            return View();
        }

        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public ColaboradorViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public JsonResult GetDoador(int id)
        {
            try
            {
                ColaboradorService service = new ColaboradorService();

                var result = service.FindById(id);

                if (result != null)
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        public JsonResult ListarTodos()
        {
            try
            {
                ColaboradorService service = new ColaboradorService();

                IList<ColaboradorViewModel> lista = new List<ColaboradorViewModel>();

                foreach (var item in service.FindAll())
                {
                    ColaboradorViewModel c = new ColaboradorViewModel();

                    c.idPessoa = item.IdPessoa;
                    c.nome = item.Nome;
                    c.diaContribuicao = item.DiaContribuicao;
                    c.valorContribuicao = item.ValorContribuicao;
                    c.periodo = item.Periodo;
                    c.cpfCnpj = item.CpfCnpj;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult Post(ColaboradorViewModel entity)
        {
            throw new NotImplementedException();
        }



        // GET: Colaborador/Details/5

    }
}
