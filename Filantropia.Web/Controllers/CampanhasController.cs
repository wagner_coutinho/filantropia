﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Entidades.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class CampanhasController : Controller, IController<CampanhaViewModel>
    {
        // GET: Campanhas
        public ActionResult Campanhas()
        {
            return View();
        }

        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public CampanhaViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public JsonResult ListarTodos()
        {
            try
            {
                CampanhaService campanhaService = new CampanhaService();

                IList<CampanhaViewModel> lista = new List<CampanhaViewModel>();

                foreach (var item in campanhaService.FindAll())
                {
                    CampanhaViewModel c = new CampanhaViewModel();

                    c.identificador = item.Identificador;
                    c.idInstituto = item.IdInstituto;
                    c.nomeCampanha = item.NomeCampanha;
                    c.descricao = item.Descricao;
                    c.inicio = item.Inicio;
                    c.fim = item.Fim;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult Post(CampanhaViewModel entity)
        {
            Campanha campanhaBase = null;

            CampanhaService campanhaService = new CampanhaService();
            try
            {
                campanhaBase = new Campanha()
                {
                    Identificador = entity.identificador,
                    IdInstituto = entity.idInstituto,
                    NomeCampanha = entity.nomeCampanha,
                    Descricao = entity.descricao,
                    Inicio = entity.inicio,
                    Fim = entity.fim
                };

                campanhaService.Save(campanhaBase);



            }
            catch (Exception e)
            {

                throw new Exception(e.Message);

            }

            return Json("Cadastrado com sucesso.");
        }
    }
}
