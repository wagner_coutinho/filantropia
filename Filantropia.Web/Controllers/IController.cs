﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public interface IController<T> where T : class
    {
        JsonResult Post(T entity);

        JsonResult Delete(int entity);

        T Get(int id);

        JsonResult ListarTodos();
    }
}
