﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Entidades.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class InstitutoController : Controller, IController<InstitutoViewModel>
    {
        // GET: Instituto
        public ActionResult Instituto()
        {
            return View();
        }

        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public InstitutoViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public JsonResult ListarTodos()
        {
            try
            {
                InstitutoService institutoService = new InstitutoService();

                IList<InstitutoViewModel> lista = new List<InstitutoViewModel>();

                foreach (var item in institutoService.FindAll())
                {
                    InstitutoViewModel c = new InstitutoViewModel();

                    c.identificador = item.Identificador;
                    c.nome = item.Nome;
                    c.cnpj = item.CNPJ;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult Post(InstitutoViewModel entity)
        {
            Instituto institutoBase = null;

            InstitutoService institutoService = new InstitutoService();
            try
            {
                institutoBase = new Instituto()
                {
                    Identificador = entity.identificador,
                    Nome = entity.nome,
                    CNPJ = entity.cnpj
                };

                institutoService.Save(institutoBase);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Json("Cadastrado com sucesso.");
        }
    }
}
