﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class TipoPessoaController : Controller, IController<TipoPessoaViewModel>
    {
        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public TipoPessoaViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        // GET: TipoPessoa
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ListarTodos()
        {
            try
            {
                TipoPessoaService tipoPessoaService = new TipoPessoaService();

                IList<TipoPessoaViewModel> lista = new List<TipoPessoaViewModel>();

                foreach (var item in tipoPessoaService.FindAll())
                {
                    TipoPessoaViewModel c = new TipoPessoaViewModel();

                    c.identificador = item.Identificador;
                    c.tipo = item.Tipo;
                    c.funcionario = item.Funcionario;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar dados! " + e.Message);
            }
        }

        public JsonResult Post(TipoPessoaViewModel entity)
        {
            throw new NotImplementedException();
        }
    }
}