﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers.Relatorio
{
    public class RelatorioLigacaoController : Controller
    {
        // GET: RelatorioLigacao
        public ActionResult RelatorioLigacao()
        {
            return View();
        }

        // GET: RelatorioLigacao/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RelatorioLigacao/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RelatorioLigacao/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RelatorioLigacao/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: RelatorioLigacao/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: RelatorioLigacao/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RelatorioLigacao/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
