﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Entidades.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Filantropia.Web.Controllers
{
    public class BancoController : Controller, IController<BancoViewModel>
    {
        // GET: Banco
        public ActionResult Banco()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public BancoViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public JsonResult ListarTodos()
        {
            try
            {
                BancoService BancoService = new BancoService();

                IList<BancoViewModel> lista = new List<BancoViewModel>();

                foreach (var item in BancoService.FindAll())
                {
                    BancoViewModel c = new BancoViewModel();

                    c.identificador = item.Identificador;
                    c.nomeBanco = item.NomeBanco;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        [HttpPost]
        public JsonResult Post(BancoViewModel entity)
        {
            Banco BancoBase = null;
            
            BancoService BancoService = new BancoService();
            try
            {
                BancoBase = new Banco()
                {
                    Identificador = entity.identificador,
                    CodigoBanco = entity.codigoBanco,
                    NomeBanco = entity.nomeBanco,
                    Agencia = entity.agencia,
                    Conta = entity.conta,
                    ChavePix = entity.chavePix
                };

                BancoService.Save(BancoBase);

                

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
                
            }

            return Json("Cadastrado com sucesso.");
        }

    }

}
