﻿using Filantropia.ClassesNegocio.Utilitarios;
using Filantropia.Entidades.Utilitarios;
using Filantropia.Util.Seguranca;
using Filantropia.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Filantropia.Web.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsuarioViewModelLogin model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AuthService rep = new AuthService();

                    Usuario u = rep.FindByEmailSenha(model.NomeUsuario, Criptografia.EncriptarMD5(model.Senha));

                    if (u != null)
                    { //gerar o objeto model que será gravado no ticket

                        UsuarioAutenticadoModel auth = new UsuarioAutenticadoModel();

                        auth.Identificador = u.Identificador;
                        auth.NomeUsuario = u.NomeUsuario;
                        auth.Email = u.Email;
                        auth.IdPerfil = u.IdPerfil;

                        //criando o ticket de autenticação..
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(JsonConvert.SerializeObject(auth), false, 5);

                        //gravar o ticket em cookie..
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));

                        //cookie.Expires = DateTime.Now.AddYears(10); NAO FAZER!!

                        Response.Cookies.Add(cookie); //gravar o cookie no navegador..

                        return RedirectToAction("Dashboard", "Dashboard", new { area = "Dashboard" });
                    }

                }
                catch (Exception)
                {

                    throw new Exception("Acesso Negado. Tente novamente.");
                }
            }

            return View();
        }

        public ActionResult Logout()
        {
            //destruir o ticket de acesso..
            FormsAuthentication.SignOut();

            //voltar para a página de login..
            return RedirectToAction("Login", "Usuario");
        }
    }
}