﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Entidades.Cadastro;
using Filantropia.Entidades.VO;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class FuncionarioController : Controller, IController<FuncionarioViewModel>
    {
        // GET: Funcionario
        public ActionResult Funcionario()
        {
            return View();
        }
        
        public JsonResult Post(FuncionarioViewModel entity)
        {
            FuncionarioVO FuncionarioBase = null;

            FuncionarioService BancoService = new FuncionarioService();
            try
            {
                FuncionarioBase = new FuncionarioVO()
                {
                    Identificador = entity.identificador,
                    DataAdmissao = entity.dataAdmissao,
                    DataDesligamento = entity.dataDesligamento,
                    Ativo = entity.ativo,
                    Bairro = entity.bairro,
                    CEP = entity.cep,
                    Cidade = entity.cidade,
                    Complemento = entity.complemento,
                    Identidade = entity.identidade,
                    IdEstadoCivil = entity.idEstadoCivil,
                    IdPessoa = entity.idPessoa,
                    IdResponsavelCadastro = entity.idResponsavelCadastro,
                    IdTipoFuncionario = entity.idTipoFuncionario,
                    Logadouro = entity.logadouro,
                    Nome = entity.nome,
                    Numero = entity.numero,
                    CpfCnpj = entity.cpfcnpj,
                    Senha = entity.senha,
                    TelefoneAlternativo = entity.telefoneAlternativo,
                    TelefoneCelular = entity.telefoneCelular,
                    TelefoneFixo = entity.telefoneFixo,
                    TipoPessoa = entity.tipoPessoa,


                };

                BancoService.Save(FuncionarioBase);

            }
            catch (Exception e)
            {

                throw new Exception(e.Message);

            }

            return Json("Cadastrado com sucesso.");
        }

        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public FuncionarioViewModel Get(int id)
        {
            throw new NotImplementedException();
        }

        public JsonResult ListarTodos()
        {
            try
            {
                FuncionarioService service = new FuncionarioService();

                IList<FuncionarioViewModel> lista = new List<FuncionarioViewModel>();

                foreach (var item in service.FindAll())
                {
                    FuncionarioViewModel c = new FuncionarioViewModel();

                    //c.idPessoa = item.IdPessoa;
                    //c. = item.Nome;
                    //c.diaContribuicao = item.DiaContribuicao;
                    //c.valorContribuicao = item.ValorContribuicao;
                    //c.periodo = item.Periodo;
                    //c.cpfCnpj = item.CpfCnpj;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar serviços! " + e.Message);
            }
        }

        public JsonResult GetFuncionario(int id)
        {
            try
            {
                FuncionarioService service = new FuncionarioService();

                var result = service.FindById(id);

                if (result != null)
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }
    }
}
