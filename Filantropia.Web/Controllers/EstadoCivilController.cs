﻿using Filantropia.ClassesNegocio.Cadastro;
using Filantropia.Entidades.Cadastro;
using Filantropia.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Filantropia.Web.Controllers
{
    public class EstadoCivilController : Controller, IController<EstadoCivil>
    {
        public JsonResult Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public EstadoCivil Get(int id)
        {
            throw new NotImplementedException();
        }

        // GET: EstadoCivil
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListarTodos()
        {
            try
            {
                EstadoCivilService estadoCivilService = new EstadoCivilService();

                IList<EstadoCivilViewModel> lista = new List<EstadoCivilViewModel>();

                foreach (var item in estadoCivilService.FindAll())
                {
                    EstadoCivilViewModel c = new EstadoCivilViewModel();

                    c.identificador = item.Identificador;
                    c.descricao = item.Descricao;

                    lista.Add(c);
                }

                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Erro ao carregar dados! " + e.Message);
            }
        }

        public JsonResult Post(EstadoCivil entity)
        {
            throw new NotImplementedException();
        }
    }
}