﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class FuncionarioViewModel
    {
        public int identificador { get; set; }
        public DateTime dataAdmissao { get; set; }
        public DateTime dataDesligamento { get; set; }
        public string identidade { get; set; }
        public int idTipoFuncionario { get; set; }
        public int idPessoa { get; set; }
        public bool ativo { get; set; }
        public string senha { get; set; }
        public int idResponsavelCadastro { get; set; }
        public string cpfcnpj { get; set; }
        public string nome { get; set; }
        public string logadouro { get; set; }
        public int numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string tipoPessoa { get; set; }
        public string telefoneFixo { get; set; }
        public string telefoneCelular { get; set; }
        public string telefoneAlternativo { get; set; }
        public int idEstadoCivil { get; set; }

    }
}