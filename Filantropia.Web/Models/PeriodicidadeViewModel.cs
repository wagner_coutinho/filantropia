﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class PeriodicidadeViewModel
    {
        public int identificador { get; set; }
        public string periodo { get; set; }
        public int meses { get; set; }
    }
}