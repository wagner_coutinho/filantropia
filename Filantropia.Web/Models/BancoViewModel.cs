﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class BancoViewModel
    {
        public int identificador { get; set; }
        public string nomeBanco { get; set; }
        public int codigoBanco { get; set; }
        public string agencia { get; set; }
        public string conta { get; set; }
        public string chavePix { get; set; }
    }
}