﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class UsuarioViewModel
    {
        public int identificador { get; set; }
        public string nomeUsuario { get; set; }
        public string senha { get; set; }
        public string email { get; set; }
        public int idPerfil { get; set; }
    }
}