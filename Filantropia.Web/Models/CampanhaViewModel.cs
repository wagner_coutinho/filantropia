﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class CampanhaViewModel
    {
        public int identificador { get; set; }
        public int idInstituto { get; set; }
        public string nomeCampanha { get; set; }
        public string descricao { get; set; }
        public DateTime inicio { get; set; }
        public DateTime fim { get; set; }
    }
}