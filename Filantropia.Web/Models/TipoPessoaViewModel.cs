﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class TipoPessoaViewModel
    {
        public int identificador { get; set; }
        public string tipo { get; set; }
        public bool funcionario { get; set; }
    }
}