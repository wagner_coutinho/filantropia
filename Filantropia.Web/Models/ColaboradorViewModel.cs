﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class ColaboradorViewModel
    {
        public int identificador { get; set; }
        public int idPessoa { get; set; }
        public DateTime dataInativacao { get; set; }
        public int diaContribuicao { get; set; }
        public decimal valorContribuicao { get; set; }
        public int idPeriodicidade { get; set; }
        public int idChamadaOriginaria { get; set; }
        public DateTime ultimaContribuicao { get; set; }
        public DateTime dataCadastro { get; set; }
        public string nome { get; set; }
        public int idTipoPessoa { get; set; }
        public int idResponsavel { get; set; }
        public int idEstadoCivil { get; set; }
        public string cpfCnpj { get; set; }
        public string periodo { get; set; }
        public string logadouro { get; set; }
        public int numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uF { get; set; }
        public string cep { get; set; }
    }
}