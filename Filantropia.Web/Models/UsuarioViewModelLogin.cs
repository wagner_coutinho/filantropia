﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class UsuarioViewModelLogin
    {
        [Display(Name = "Usuario:")] 
        [Required(ErrorMessage = "Por favor, informe o Usuario de acesso.")] 
        public string NomeUsuario { get; set; }

        [Display(Name = "Senha:")] 
        [Required(ErrorMessage = "Por favor, informe a senha de acesso.")] 
        [DataType(DataType.Password)] 
        public string Senha { get; set; }
    }
}