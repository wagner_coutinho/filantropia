﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class InstitutoViewModel
    {
        public int identificador { get; set; }
        public string nome { get; set; }
        public string cnpj { get; set; }
    }
}