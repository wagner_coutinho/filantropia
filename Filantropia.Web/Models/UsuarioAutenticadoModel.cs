﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Filantropia.Web.Models
{
    public class UsuarioAutenticadoModel
    {
        public int Identificador { get; set; }
        public string NomeUsuario { get; set; } 
        public string Email { get; set; }
        public int IdPerfil { get; set; } 
    }
}