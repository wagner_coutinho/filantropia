﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.VO
{
    public class ColaboradorVO
    {
        public int Identificador { get; set; }
        public int IdPessoa { get; set; }
        public DateTime DataInativacao { get; set; }
        public int DiaContribuicao { get; set; }
        public decimal ValorContribuicao { get; set; }
        public int IdPeriodicidade { get; set; }
        public int IdChamadaOriginaria { get; set; }
        public DateTime UltimaContribuicao { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Nome { get; set; }
        public int IdTipoPessoa { get; set; }
        public int IdResponsavel { get; set; }
        public int IdEstadoCivil { get; set; }
        public string CpfCnpj { get; set; }
        public string Periodo { get; set; }
        public string Logadouro { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }

    }
}
