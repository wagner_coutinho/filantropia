﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.VO
{
    public class FuncionarioVO
    {
        public int Identificador { get; set; }
        public DateTime DataAdmissao { get; set; }
        public DateTime DataDesligamento { get; set; }
        public string Identidade { get; set; }
        public int IdTipoFuncionario { get; set; }
        public int IdPessoa { get; set; }
        public bool Ativo { get; set; }
        public string Senha { get; set; }
        public int IdResponsavelCadastro { get; set; }
        public string CpfCnpj { get; set; }
        public string Nome { get; set; }
        public string Logadouro { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string CEP { get; set; }
        public string TipoPessoa { get; set; }
        public string TelefoneFixo { get; set; }
        public string TelefoneCelular { get; set; }
        public string TelefoneAlternativo { get; set; }
        public int IdEstadoCivil { get; set; }
    }
}
