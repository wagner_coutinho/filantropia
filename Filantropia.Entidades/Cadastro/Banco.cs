﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Banco
    {
        public int Identificador { get; set; }
        public string NomeBanco { get; set; }
        public int CodigoBanco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string ChavePix { get; set; }
    }
}
