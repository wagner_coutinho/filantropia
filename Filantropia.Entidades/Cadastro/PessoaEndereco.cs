﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class PessoaEndereco
    {
        public int Identificador { get; set; }
        public int IdPessoa { get; set; }
        public int IdTelefone { get; set; }
        public int IdEndereco { get; set; }
        public int IdTipoEndereco { get; set; }
        public DateTime DataRegistro { get; set; }
        public int IDResponsavelCadastro { get; set; }
        public bool Padrao { get; set; }
    }
}
