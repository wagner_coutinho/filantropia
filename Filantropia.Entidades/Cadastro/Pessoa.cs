﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Pessoa
    {
        public int Identificador { get; set; }
        public string Nome { get; set; }
        public int IdTipoPessoa { get; set; }
        public int IdResponsavel { get; set; }
        public int IdEstadoCivil { get; set; }
        public string CpfCnpj { get; set; }
    }
}
