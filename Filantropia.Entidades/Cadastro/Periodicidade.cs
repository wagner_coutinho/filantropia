﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Periodicidade
    {
        public int Identificador { get; set; }
        public string Periodo { get; set; }
        public int Meses { get; set; }
    }
}
