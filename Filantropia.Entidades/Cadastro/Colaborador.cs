﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Colaborador
    {
        public int Identificador { get; set; }
        public int IdPessoa { get; set; }
        public DateTime DataInativacao { get; set; }
        public int DiaContribuicao { get; set; }
        public decimal ValorContribuicao { get; set; }
        public int IdPeriodicidade { get; set; }
        public int IdChamadaOriginaria { get; set; }
        public DateTime UltimaContribuicao { get; set; }
        public DateTime DataCadastro { get; set; }

    }
}
