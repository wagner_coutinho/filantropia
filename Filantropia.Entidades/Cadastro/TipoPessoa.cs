﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class TipoPessoa
    {
        public int Identificador { get; set; }
        public string Tipo { get; set; }
        public bool Funcionario { get; set; }
    }
}
