﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class PessoaTelefone
    {
        public int Identificador { get; set; }
        public int IdPessoa { get; set; }
        public int IdTelefone { get; set; }
        public bool SMS { get; set; }
        public bool Whatsapp { get; set; }
        public int IDTipoTelefone { get; set; }
        public int IDContato { get; set; }
        public DateTime DataRegistro { get; set; }
        public  int IDResponsavelCadastro { get; set; }
        public DateTime DataDesativacao { get; set; }
        public string Motivo { get; set; }
        public bool Padrao { get; set; }



    }
}
