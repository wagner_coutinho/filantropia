﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Campanha
    {
        public int Identificador { get; set; }
        public int IdInstituto { get; set; }
        public string NomeCampanha { get; set; }
        public string Descricao { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }

    }
}
