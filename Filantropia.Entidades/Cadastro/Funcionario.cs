﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Funcionario
    {
        public int Identificador { get; set; }
        public DateTime DataAdmissao { get; set; }
        public DateTime DataDesligamento { get; set; }
        public string Identidade { get; set; }
        public int IdTipoFuncionario { get; set; }
        public int IdPessoa { get; set; }
        public bool Ativo { get; set; }
        public string Senha { get; set; }
        public int IdResponsavelCadastro { get; set; }
    }
}
