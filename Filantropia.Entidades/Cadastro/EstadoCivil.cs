﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class EstadoCivil
    {
        public int Identificador { get; set; }
        public string Descricao { get; set; }
    }
}
