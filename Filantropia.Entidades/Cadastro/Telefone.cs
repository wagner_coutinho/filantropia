﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.Entidades.Cadastro
{
    public class Telefone
    {
        public int Identificador { get; set; }
        public int DDD { get; set; }
        public string Prefixo { get; set; }
        public string Sufixo { get; set; }
    }
}
