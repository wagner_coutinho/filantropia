﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    
    public class BancoService : IService<Banco>
    {
        
        public void Save(Banco entity)
        {
            BancoRepository bancoRepository = new BancoRepository();
            try
            {
                Banco bancoBase = bancoRepository.FindById(entity.Identificador);

                if (bancoBase == null)
                {
                    bancoRepository.Insert(entity);
                }
                else
                {
                    bancoRepository.Update(entity);
                }
                
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            BancoRepository bancoRepository = new BancoRepository();
            try
            {
                bancoRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Banco> FindAll()
        {
            BancoRepository bancoRepository = new BancoRepository();

            List<Banco> listBanco = new List<Banco>();
            try
            {
                listBanco = bancoRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public Banco FindById(int id)
        {
            BancoRepository bancoRepository = new BancoRepository();

            Banco banco = new Banco();

            try
            {
                banco = bancoRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return banco;
        
        }

        public Banco FindByName(string name)
        {
            BancoRepository bancoRepository = new BancoRepository();
            Banco banco = new Banco();

            try
            {
                banco = bancoRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return banco;
        }

       
    }
}
