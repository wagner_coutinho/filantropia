﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class TelefoneService : IService<Telefone>
    {
        public void Save(Telefone entity)
        {
            TelefoneRepository TelefoneRepository = new TelefoneRepository();
            try
            {
                Telefone TelefoneBase = TelefoneRepository.FindById(entity.Identificador);

                if (TelefoneBase == null)
                {
                    TelefoneRepository.Insert(entity);
                }
                else
                {
                    TelefoneRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            TelefoneRepository TelefoneRepository = new TelefoneRepository();
            try
            {
                TelefoneRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Telefone> FindAll()
        {
            TelefoneRepository TelefoneRepository = new TelefoneRepository();

            List<Telefone> listBanco = new List<Telefone>();
            try
            {
                listBanco = TelefoneRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public Telefone FindById(int id)
        {
            TelefoneRepository TelefoneRepository = new TelefoneRepository();

            Telefone Telefone = new Telefone();

            try
            {
                Telefone = TelefoneRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Telefone;
        }

        public Telefone FindByName(string name)
        {
            TelefoneRepository TelefoneRepository = new TelefoneRepository();

            Telefone Telefone = new Telefone();

            try
            {
                Telefone = TelefoneRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Telefone;
        }
    }
}
