﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class TipoPessoaService : IService<TipoPessoa>
    {
        public void Save(TipoPessoa entity)
        {
            TipoPessoaRepository TipoPessoaRepository = new TipoPessoaRepository();
            try
            {
                TipoPessoa TipoPessoaBase = TipoPessoaRepository.FindById(entity.Identificador);

                if (TipoPessoaBase == null)
                {
                    TipoPessoaRepository.Insert(entity);
                }
                else
                {
                    TipoPessoaRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            TipoPessoaRepository TipoPessoaRepository = new TipoPessoaRepository();
            try
            {
                TipoPessoaRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<TipoPessoa> FindAll()
        {
            TipoPessoaRepository TipoPessoaRepository = new TipoPessoaRepository();

            List<TipoPessoa> listBanco = new List<TipoPessoa>();
            try
            {
                listBanco = TipoPessoaRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public TipoPessoa FindById(int id)
        {
            TipoPessoaRepository TipoPessoaRepository = new TipoPessoaRepository();

            TipoPessoa TipoPessoa = new TipoPessoa();

            try
            {
                TipoPessoa = TipoPessoaRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return TipoPessoa;
        }

        public TipoPessoa FindByName(string name)
        {
            TipoPessoaRepository TipoPessoaRepository = new TipoPessoaRepository();

            TipoPessoa TipoPessoa = new TipoPessoa();

            try
            {
                TipoPessoa = TipoPessoaRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return TipoPessoa;
        }
    }
}
