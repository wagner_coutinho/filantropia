﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class PessoaService : IService<Pessoa>
    {
        public void Save(Pessoa entity)
        {
            PessoaRepository PessoaRepository = new PessoaRepository();
            try
            {
                Pessoa PessoaBase = PessoaRepository.FindById(entity.Identificador);

                if (PessoaBase == null)
                {
                    PessoaRepository.Insert(entity);
                }
                else
                {
                    PessoaRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            PessoaRepository PessoaRepository = new PessoaRepository();
            try
            {
                PessoaRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Pessoa> FindAll()
        {
            PessoaRepository PessoaRepository = new PessoaRepository();

            List<Pessoa> listBanco = new List<Pessoa>();
            try
            {
                listBanco = PessoaRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public Pessoa FindById(int id)
        {
            PessoaRepository PessoaRepository = new PessoaRepository();

            Pessoa Pessoa = new Pessoa();

            try
            {
                Pessoa = PessoaRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Pessoa;
        }

        public Pessoa FindByName(string name)
        {
            PessoaRepository PessoaRepository = new PessoaRepository();

            Pessoa Pessoa = new Pessoa();

            try
            {
                Pessoa = PessoaRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Pessoa;
        }
    }
}
