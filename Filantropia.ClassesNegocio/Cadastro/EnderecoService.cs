﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class EnderecoService : IService<Endereco>
    {
        public void Save(Endereco entity)
        {
            EnderecoRepository EnderecoRepository = new EnderecoRepository();
            try
            {
                Endereco EnderecoBase = EnderecoRepository.FindById(entity.Identificador);

                if (EnderecoBase == null)
                {
                    EnderecoRepository.Insert(entity);
                }
                else
                {
                    EnderecoRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            EnderecoRepository EnderecoRepository = new EnderecoRepository();
            try
            {
                EnderecoRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Endereco> FindAll()
        {
            EnderecoRepository EnderecoRepository = new EnderecoRepository();

            List<Endereco> listBanco = new List<Endereco>();
            try
            {
                listBanco = EnderecoRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public Endereco FindById(int id)
        {
            EnderecoRepository EnderecoRepository = new EnderecoRepository();

            Endereco Endereco = new Endereco();

            try
            {
                Endereco = EnderecoRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Endereco;
        }

        public Endereco FindByName(string name)
        {
            EnderecoRepository EnderecoRepository = new EnderecoRepository();

            Endereco Endereco = new Endereco();

            try
            {
                Endereco = EnderecoRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Endereco;
        }

    }
}
