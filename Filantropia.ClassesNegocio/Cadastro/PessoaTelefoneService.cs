﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using Filantropia.Persistencia.Repositories;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class PessoaTelefoneService : IService<PessoaTelefone>
    {
        public void Save(PessoaTelefone entity)
        {
            PessoaTelefoneRepository PessoaTelefoneRepository = new PessoaTelefoneRepository();
            try
            {
                PessoaTelefone PessoaTelefoneBase = PessoaTelefoneRepository.FindById(entity.Identificador);

                if (PessoaTelefoneBase == null)
                {
                    PessoaTelefoneRepository.Insert(entity);
                }
                else
                {
                    PessoaTelefoneRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            PessoaTelefoneRepository PessoaTelefoneRepository = new PessoaTelefoneRepository();
            try
            {
                PessoaTelefoneRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<PessoaTelefone> FindAll()
        {
            PessoaTelefoneRepository PessoaTelefoneRepository = new PessoaTelefoneRepository();

            List<PessoaTelefone> listBanco = new List<PessoaTelefone>();
            try
            {
                listBanco = PessoaTelefoneRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public PessoaTelefone FindById(int id)
        {
            PessoaTelefoneRepository PessoaTelefoneRepository = new PessoaTelefoneRepository();

            PessoaTelefone PessoaTelefone = new PessoaTelefone();

            try
            {
                PessoaTelefone = PessoaTelefoneRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return PessoaTelefone;
        }

        public PessoaTelefone FindByName(string name)
        {
            PessoaTelefoneRepository PessoaTelefoneRepository = new PessoaTelefoneRepository();

            PessoaTelefone PessoaTelefone = new PessoaTelefone();

            try
            {
                PessoaTelefone = PessoaTelefoneRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return PessoaTelefone;
        }
    }
}
