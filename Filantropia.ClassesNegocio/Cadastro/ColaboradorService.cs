﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Entidades.VO;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class ColaboradorService : IService<ColaboradorVO>
    {
        public void Save(ColaboradorVO entity)
        {
            ColaboradorRepository colaboradorRepository = new ColaboradorRepository();
            try
            {
                ColaboradorVO colaboradorBase = colaboradorRepository.FindById(entity.Identificador);

                if (colaboradorBase == null)
                {
                    colaboradorRepository.Insert(entity);
                }
                else
                {
                    colaboradorRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            ColaboradorRepository colaboradorRepository = new ColaboradorRepository();
            try
            {
                colaboradorRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<ColaboradorVO> FindAll()
        {
            ColaboradorRepository colaboradorRepository = new ColaboradorRepository();

            List<ColaboradorVO> listBanco = new List<ColaboradorVO>();
            try
            {
                listBanco = colaboradorRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public ColaboradorVO FindById(int id)
        {
            ColaboradorRepository colaboradorRepository = new ColaboradorRepository();
            EnderecoRepository enderecoRepository = new EnderecoRepository();

            ColaboradorVO colaborador = new ColaboradorVO();
            Endereco endereco = new Endereco();

            try
            {
                colaborador = colaboradorRepository.FindById(id);

                endereco = enderecoRepository.FindByIdPessoa(colaborador.IdPessoa);

                if (endereco != null)
                {
                    colaborador.Logadouro = endereco.Logadouro;
                    colaborador.Numero = endereco.Numero;
                    colaborador.CEP = endereco.CEP;
                    colaborador.Bairro = endereco.Bairro;
                    colaborador.Cidade = endereco.Cidade;
                    colaborador.Complemento = endereco.Complemento;
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return colaborador;
        }

        public ColaboradorVO FindByName(string name)
        {
            ColaboradorRepository colaboradorRepository = new ColaboradorRepository();

            ColaboradorVO colaborador = new ColaboradorVO();

            try
            {
                colaborador = colaboradorRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return colaborador;
        }


    }
}
