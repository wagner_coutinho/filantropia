﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class EstadoCivilService : IService<EstadoCivil>
    {
        public void Save(EstadoCivil entity)
        {
            EstadoCivilRepository EstadoCivilRepository = new EstadoCivilRepository();
            try
            {
                EstadoCivil EstadoCivilBase = EstadoCivilRepository.FindById(entity.Identificador);

                if (EstadoCivilBase == null)
                {
                    EstadoCivilRepository.Insert(entity);
                }
                else
                {
                    EstadoCivilRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            EstadoCivilRepository EstadoCivilRepository = new EstadoCivilRepository();
            try
            {
                EstadoCivilRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<EstadoCivil> FindAll()
        {
            EstadoCivilRepository EstadoCivilRepository = new EstadoCivilRepository();

            List<EstadoCivil> listBanco = new List<EstadoCivil>();
            try
            {
                listBanco = EstadoCivilRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public EstadoCivil FindById(int id)
        {
            EstadoCivilRepository EstadoCivilRepository = new EstadoCivilRepository();

            EstadoCivil EstadoCivil = new EstadoCivil();

            try
            {
                EstadoCivil = EstadoCivilRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return EstadoCivil;
        }

        public EstadoCivil FindByName(string name)
        {
            EstadoCivilRepository EstadoCivilRepository = new EstadoCivilRepository();

            EstadoCivil EstadoCivil = new EstadoCivil();

            try
            {
                EstadoCivil = EstadoCivilRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return EstadoCivil;
        }
    }
}
