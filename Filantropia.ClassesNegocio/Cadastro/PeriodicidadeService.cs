﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class PeriodicidadeService : IService<Periodicidade>
    {
        public void Save(Periodicidade entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int entity)
        {
            throw new NotImplementedException();
        }

        public List<Periodicidade> FindAll()
        {
            List<Periodicidade> listPeriodicidade = new List<Periodicidade>();
            PeriodicidadeRepository repository = new PeriodicidadeRepository();

            try
            {
                listPeriodicidade = repository.FindAll();

                return listPeriodicidade;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Periodicidade FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Periodicidade FindByName(string name)
        {
            throw new NotImplementedException();
        }

    }
}
