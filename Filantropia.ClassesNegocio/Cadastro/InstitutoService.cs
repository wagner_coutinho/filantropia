﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class InstitutoService : IService<Instituto>
    {
        public void Save(Instituto entity)
        {
            InstitutoRepository institutoRepository = new InstitutoRepository();
            try
            {
                
                Instituto institutoBase = institutoRepository.FindById(entity.Identificador);

                if (institutoBase == null)
                {
                    institutoRepository.Insert(entity);
                }
                else
                {
                    institutoRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            InstitutoRepository institutoRepository = new InstitutoRepository();
            try
            {
                institutoRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Instituto> FindAll()
        {
            InstitutoRepository institutoRepository = new InstitutoRepository();

            List<Instituto> listInstituto = new List<Instituto>();
            try
            {
                listInstituto = institutoRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listInstituto;
        }

        public Instituto FindById(int id)
        {
            InstitutoRepository institutoRepository = new InstitutoRepository();

            Instituto instituto = new Instituto();

            try
            {
                instituto = institutoRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return instituto;
        }

        public Instituto FindByName(string name)
        {
            InstitutoRepository institutoRepository = new InstitutoRepository();

            Instituto instituto = new Instituto();

            try
            {
                instituto = institutoRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return instituto;
        }

       
    }
}
