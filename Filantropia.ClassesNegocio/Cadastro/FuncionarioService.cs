﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Entidades.VO;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class FuncionarioService : IService<FuncionarioVO>
    {
        public void Save(FuncionarioVO entity)
        {
            FuncionarioRepository FuncionarioRepository = new FuncionarioRepository();
            PessoaRepository PessoaRepository = new PessoaRepository();
            EnderecoRepository EnderecoRepository = new EnderecoRepository();
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();

            Pessoa pessoa = new Pessoa();
            Pessoa pessoaBase = new Pessoa();
            PessoaEndereco pessoaEndereco = new PessoaEndereco();
            Endereco endereco = new Endereco();
            Endereco enderecoBase = new Endereco();

            try
            {
                FuncionarioVO FuncionarioBase = FuncionarioRepository.FindById(entity.Identificador);

                if (FuncionarioBase == null)
                {
                   

                    pessoa.Nome = entity.Nome;
                    pessoa.IdTipoPessoa = Convert.ToInt32(entity.TipoPessoa);
                    pessoa.CpfCnpj = entity.CpfCnpj;
                    pessoa.IdResponsavel = entity.IdResponsavelCadastro;
                    pessoa.IdEstadoCivil = entity.IdEstadoCivil;

                    PessoaRepository.Insert(pessoa);

                    endereco.Logadouro = entity.Logadouro;
                    endereco.Numero = entity.Numero;
                    endereco.Bairro = entity.Bairro;
                    endereco.Cidade = entity.Cidade;
                    endereco.Complemento = entity.Complemento;

                    EnderecoRepository.Insert(endereco);

                    enderecoBase = EnderecoRepository.FindByEnderecoAndNumeroAndComplemento(entity.Logadouro, entity.Numero, entity.Complemento);
                    pessoaBase

                    if (endere)
                    {

                    }






                    pessoaBase = PessoaRepository.FindByName(pessoa.Nome);
                    enderecoBase = EnderecoRepository.FindByIdPessoa(pessoaBase.Identificador);









                    FuncionarioRepository.Insert(entity);


                }
                else
                {
                    FuncionarioRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            FuncionarioRepository FuncionarioRepository = new FuncionarioRepository();
            try
            {
                FuncionarioRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<FuncionarioVO> FindAll()
        {
            FuncionarioRepository FuncionarioRepository = new FuncionarioRepository();

            List<FuncionarioVO> listBanco = new List<FuncionarioVO>();
            try
            {
                listBanco = FuncionarioRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public FuncionarioVO FindById(int id)
        {
            FuncionarioRepository FuncionarioRepository = new FuncionarioRepository();

            FuncionarioVO Funcionario = new FuncionarioVO();

            try
            {
                Funcionario = FuncionarioRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Funcionario;
        }

        public FuncionarioVO FindByName(string name)
        {
            FuncionarioRepository FuncionarioRepository = new FuncionarioRepository();

            FuncionarioVO Funcionario = new FuncionarioVO();

            try
            {
                Funcionario = FuncionarioRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return Funcionario;
        }
    }
}
