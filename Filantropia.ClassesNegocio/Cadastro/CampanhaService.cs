﻿using Filantropia.Entidades.Cadastro;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class CampanhaService : IService<Campanha>
    {
        public void Save(Campanha entity)
        {
            CampanhaRepository campanhaRepository = new CampanhaRepository();
            try
            {
                Campanha campanhaBase = campanhaRepository.FindById(entity.Identificador);

                if (campanhaBase == null)
                {
                    campanhaRepository.Insert(entity);
                }
                else
                {
                    campanhaRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public void Delete(int entity)
        {
            CampanhaRepository campanhaRepository = new CampanhaRepository();
            try
            {
                campanhaRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Campanha> FindAll()
        {
            CampanhaRepository campanhaRepository = new CampanhaRepository();

            List<Campanha> listCampanha = new List<Campanha>();
            try
            {
                listCampanha = campanhaRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listCampanha;
        }

        public Campanha FindById(int id)
        {
            CampanhaRepository campanhaRepository = new CampanhaRepository();

            Campanha campanha = new Campanha();

            try
            {
                campanha = campanhaRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return campanha;
        }

        public Campanha FindByName(string name)
        {
            CampanhaRepository campanhaRepository = new CampanhaRepository();

            Campanha campanha = new Campanha();

            try
            {
                campanha = campanhaRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return campanha;
        }


    }
}
