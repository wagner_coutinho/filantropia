﻿using Filantropia.Entidades.Cadastro;
using System;
using System.Collections.Generic;
using Filantropia.Persistencia.Repositories;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Cadastro
{
    public class PessoaEnderecoService : IService<PessoaEndereco>
    {
        public void Save(PessoaEndereco entity)
        {
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();
            try
            {
                PessoaEndereco PessoaEnderecoBase = PessoaEnderecoRepository.FindById(entity.Identificador);

                if (PessoaEnderecoBase == null)
                {
                    PessoaEnderecoRepository.Insert(entity);
                }
                else
                {
                    PessoaEnderecoRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();
            try
            {
                PessoaEnderecoRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<PessoaEndereco> FindAll()
        {
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();

            List<PessoaEndereco> listBanco = new List<PessoaEndereco>();
            try
            {
                listBanco = PessoaEnderecoRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listBanco;
        }

        public PessoaEndereco FindById(int id)
        {
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();

            PessoaEndereco PessoaEndereco = new PessoaEndereco();

            try
            {
                PessoaEndereco = PessoaEnderecoRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return PessoaEndereco;
        }

        public PessoaEndereco FindByName(string name)
        {
            PessoaEnderecoRepository PessoaEnderecoRepository = new PessoaEnderecoRepository();

            PessoaEndereco PessoaEndereco = new PessoaEndereco();

            try
            {
                PessoaEndereco = PessoaEnderecoRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return PessoaEndereco;
        }
    }
}
