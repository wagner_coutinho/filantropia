﻿using Filantropia.Entidades.Utilitarios;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Utilitarios
{
    public class AuthService
    {
        public Usuario FindByEmailSenha(string usuario, string senha)
        {
            AuthRepository usuarioRepository = new AuthRepository();

            Usuario usuarioBase = new Usuario();

            try
            {
                usuarioBase = usuarioRepository.FindByLoginSenha(usuario, senha);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return usuarioBase;
        }
    }
}
