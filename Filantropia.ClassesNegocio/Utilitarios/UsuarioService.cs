﻿using Filantropia.Entidades.Utilitarios;
using Filantropia.Persistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio.Utilitarios
{
    public class UsuarioService : IService<Usuario>
    {
        public void Save(Usuario entity)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();
            try
            {
                Usuario  UsuarioBase = usuarioRepository.FindById(entity.Identificador);

                if (UsuarioBase == null)
                {
                    usuarioRepository.Insert(entity);
                }
                else
                {
                    usuarioRepository.Update(entity);
                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int entity)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();
            try
            {
                usuarioRepository.Delete(entity);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public List<Usuario> FindAll()
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();

            List<Usuario> listUsuario = new List<Usuario>();
            try
            {
                listUsuario = usuarioRepository.FindAll();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return listUsuario;
        }

        public Usuario FindById(int id)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();

            Usuario usuario = new Usuario();

            try
            {
                usuario = usuarioRepository.FindById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return usuario;
        }

        public Usuario FindByName(string name)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();

            Usuario usuario = new Usuario();

            try
            {
                usuario = usuarioRepository.FindByName(name);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return usuario;
        }




    }
}
