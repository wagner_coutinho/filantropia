﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filantropia.ClassesNegocio
{
    internal interface IService<T> where T : class
    {
        void Save(T entity);

        void Delete(int entity);

        T FindByName(string name);

        T FindById(int id);

        List<T> FindAll();
    }
}
